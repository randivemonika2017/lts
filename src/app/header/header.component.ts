import { Component, OnInit } from '@angular/core';
import {HidebannerService} from '../cms-pages/hidebanner.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private bannerservice:HidebannerService) { }

  ngOnInit(): void {
  }
  hideSlider(condition:boolean){
    if(condition){
      this.bannerservice.bannerSource.next(true);
    }else{
      this.bannerservice.bannerSource.next(false);
    }
  }
}

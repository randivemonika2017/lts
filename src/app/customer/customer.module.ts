import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { YouTubePlayerModule } from "@angular/youtube-player";
import { CustomerRoutingModule } from './customer-routing.module';
//import { SubCategoryComponent } from './sub-category/sub-category.component';
//import { BusinessListComponent } from './business-list/business-list.component';
import { BusinessProfileComponent } from '../business-profile/business-profile.component';
import { AddListingComponent } from './add-listing/add-listing.component';
//import { BusinessListGridComponent } from './business-list-grid/business-list-grid.component';
import { from } from 'rxjs';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import {GoogleMapsModule} from '@angular/google-maps';
import { FileUploadModule } from 'ng2-file-upload';

@NgModule({
  declarations: [  AddListingComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    CustomerRoutingModule,
    ReactiveFormsModule,
    NgMultiSelectDropDownModule,
    YouTubePlayerModule,
    GoogleMapsModule,
    FileUploadModule

  ],
providers:[FormBuilder]
})
export class CustomerModule { }

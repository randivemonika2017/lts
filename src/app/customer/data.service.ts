import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpBackend } from '@angular/common/http';
  
import { Observable,throwError } from 'rxjs'; 
import { catchError } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class DataService {
  public url= 'http://127.0.0.1:8000/api/';
  public apiUrl = 'http://localtradestreet.com/ltsapi/public/api/';
  private http: HttpClient;

constructor(handler: HttpBackend) {
    this.http = new HttpClient(handler);
}

//   getcategories():Observable<any> {
//     return this.http.get(`${this.apiUrl}cat`);
// }
getcategories():Observable<any> {
  return this.http.get('http://127.0.0.1:8000/api/cat');
}

  getSubCategories(category:string){
    // let param1=new HttpParams().set('cat_id',category);
    return this.http.get(`${this.apiUrl}subcatname/`+category);
  }
  getSubcategories(cat_id:number){
    return this.http.get(`${this.apiUrl}subid/`+cat_id);
  }
  getAllSubCategories(){
  return this.http.get(`${this.apiUrl}subcat`);
  }

  getBusinessList(subcat: string) {
    return this.http.get(`${this.apiUrl}businesses/`+subcat);
  }
  getBusiness(title:string){
     return this.http.get(`${this.apiUrl}business/`+title);
  }

  deleteCategory(id:number){
    return this.http.delete(`${this.apiUrl}deleteCat/`+id)
    
  }
  deleteSubcategory(id:number){
    return this.http.delete(`${this.apiUrl}deleteSubCat/`+id)
  }
  deleteBusiness(id:number){
    return this.http.delete(`${this.apiUrl}deleteBusiness/`+id)
  }   
  // getCurrentCategory(id:any){
  //   return this.http.get(`${this.apiUrl}catbyid/`+id)
  // }
  getCurrentCategory(id:any){
    return this.http.get('http://127.0.0.1:8000/api/catbyid/'+id)
  }
  // UpdateCategory(id:any,fd:any) {
    
  //   return this.http.put(`${this.apiUrl}updatecat/`+id,fd)
  // }
  UpdateCategory(id:any,fd:any) {
    
    return this.http.put('http://127.0.0.1:8000/api/updatecat/'+id,fd)
  }
  getCurrentSubcategory(id:any){
    return this.http.get(`${this.apiUrl}subcatbyid/`+id)
  }
  UpdateSubcategory(id:any,fd:any) {
    
    return this.http.put(`${this.apiUrl}updatesubcat/`+id,fd)
  }

  getAllBusinesses(){
    return this.http.get(`${this.apiUrl}business`);
  }
  submitListing(fd:any){
    return this.http.post(`${this.apiUrl}business`,fd);
  }
  register(fd: any) {
    return this.http.post(`${this.url}sales`, fd);
  }
  upload(formData:any){
    return this.http.post(`${this.apiUrl}business`,formData);
  }

  // getStates() {
  //   return this.http.get(`${this.apiUrl}api/states`).pipe(
  //     catchError(this.handleError)
  //   );
  // }

  // getCities(stateId: number) {
  //   return this.http.get(`${this.apiUrl}api/cities/${stateId}`).pipe(
  //     catchError(this.handleError)
  //   );
  // }


 
  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError('Something bad happened. Please try again later.');
  }
}
//   getCategory()
//   {
//     return this._http.get(this.testURL);
//   }
//   getCurrentCategory(id:any){
//     return this._http.get(`${this.testURL}/${id}`)
//   }
// savelisting(data: any)
// {
//   return this._http.post(this.testURL,data)
//   }

 
//   getSubcategory()
//   {
//     return this._http.get(this.testURL2);
//   }
//   //   getTypes(): Observable<Category[]> {
//   //   const headers = new HttpHeaders({'category': 'category1'});
//   //   return this._http.get<Category[]>(this.testURL, {headers});
//   // }
// }

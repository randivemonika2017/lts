import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SubCategoryComponent } from '../sub-category/sub-category.component';
import { AddListingComponent } from './add-listing/add-listing.component';
//import { BusinessListGridComponent } from './business-list-grid/business-list-grid.component';
//import { BusinessListComponent } from './business-list/business-list.component'
import { BusinessProfileComponent } from '../business-profile/business-profile.component';


const routes: Routes = [

  // {path: 'category/:id', component: SubCategoryComponent},
  // {path: ':id/:id', component:BusinessListComponent},
  //    {path:'category',component:CategoryComponent},
  // {path:'category/:category', component:SubCategoryComponent},
  // // {path: ':name/:name', component:BusinessListComponent},
  // {path:':name/:name/:name', component:BusinessProfileComponent},
  {path:'addListing', component:AddListingComponent},
  // {path:'Businesslist-listview', component:BusinessListComponent},
  // {path:'Businesslist-gridview', component:BusinessListGridComponent},s
  // {path:'business-profile', component:BusinessProfileComponent}
 
 // {path:'category/:subcat', component:SubCategoryComponent},
 
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CustomerRoutingModule { }

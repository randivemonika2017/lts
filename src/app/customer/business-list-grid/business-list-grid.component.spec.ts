import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BusinessListGridComponent } from './business-list-grid.component';

describe('BusinessListGridComponent', () => {
  let component: BusinessListGridComponent;
  let fixture: ComponentFixture<BusinessListGridComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BusinessListGridComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusinessListGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

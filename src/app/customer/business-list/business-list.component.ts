import { Component, OnInit } from '@angular/core';
import {  ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { DataService } from '../data.service';
// import { CardService } from '../card.service';
// import { DataService } from '../customer/data.service';

@Component({
  selector: 'app-business-list',
  templateUrl: './business-list.component.html',
  styleUrls: ['./business-list.component.css']
})
export class BusinessListComponent implements OnInit {
  businesses: any={};
  
url =environment;
  para: any;
  selectedBusiness: any;
  constructor(private dataService:DataService,private router:Router,private route:ActivatedRoute){
    this.route.params.subscribe((dt)=>{
      this.para = dt;
      console.log(this.para);
    })
  }
  ngOnInit(): void {

    this.dataService.getBusinessList(this.para.subcat).subscribe(
      data => this.businesses = data
    );

  }
  onSelectBusiness(business: any){
  
   
    if (business) {
      this.dataService.getBusiness(business).subscribe(
        data => {
          this.businesses = data;
        
        }
      );
    } else {
      this.businesses = null;
    
    }
    // this.selectedBusiness = business;

    // this.router.navigateByUrl("/business/" + business.title);
 
    this.router.navigate([":subcat/:title", business.title]);
 }


}

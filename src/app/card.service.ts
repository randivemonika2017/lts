import { Injectable } from '@angular/core';

import { HttpClient, HttpErrorResponse,HttpHeaders  } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class CardService {

// ServerUrl = 'http://localhost/dev/blogger/';
// errorData = {};

// httpOptions = {
//       headers: new HttpHeaders({'Content-Type': 'application/json'})
//     };
  
// constructor(private http: HttpClient) { }

// addListingForm(formdata: Addlisting) {
//       return this.http.post<Addlisting>(this.ServerUrl + 'api/contact', formdata, this.httpOptions).pipe(
//         catchError(this.handleError)
//       );
//     }


//   getAllCategories() {
//     return this.http.get<Category>(this.ServerUrl + 'api/all_categories').pipe(
//       catchError(this.handleError)
//     );
//   }

//   getAllSubCategories(id: number) {
//       return this.http.get<Subcategory>(this.ServerUrl + 'api/all_subcategories/'+id).pipe(
//         catchError(this.handleError)
//       );
//     }
//     getBusinessList(id: number) {
//       return this.http.get<BusinessList>(this.ServerUrl + 'api/business_list/'+id).pipe(
//         catchError(this.handleError)
//       );
//     }

 

//   private handleError(error: HttpErrorResponse) {
//     if (error.error instanceof ErrorEvent) {

//       // A client-side or network error occurred. Handle it accordingly.

//       console.error('An error occurred:', error.error.message);
//     } else {

//       // The backend returned an unsuccessful response code.

//       // The response body may contain clues as to what went wrong,

//       console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
//     }

//     // return an observable with a user-facing error message

//     this.errorData = {
//       errorTitle: 'Oops! Request for document failed',
//       errorDesc: 'Something bad happened. Please try again later.'
//     };
//     return throwError(this.errorData);
//   }









  name: any;

  cards = [
    {id: 1, img:"assets/images/category/beauty.png", name: "Beauty",
     sub: [{id:1,img:"assets/images/category/beauty/a1.png",name:"Beauty Parlors",
            sub1:[{id:1, img:'assets/images/category/beauty/a1/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review",
                  sub2:[{id:1, bgimg:'assets/images/category/beauty/a1/aa1.jpg',img:'/assets/images/home/hero.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review",email:"anilwadhwani1111@yahoo.in",description:"Jarry Profit is one of the best Builders and Developers in Jaripatka, Nagpur. It offers residential apartments, flats, bungalow, Row house, residential plots, commercial properties and more"}
                  ],
                  },
                  {id:2, img:'assets/images/category/beauty/a1/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                  {id:3, img:'assets/images/category/beauty/a1/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                  {id:4, img:'assets/images/category/beauty/a1/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:5, img:'assets/images/category/beauty/a1/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                ]
            },
            {id:2,img:"assets/images/category/beauty/a2.png",name:"Spa Center",
              sub1:[{id:1, img:'assets/images/category/beauty/a2/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:2, img:'assets/images/category/beauty/a2/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                    {id:3, img:'assets/images/category/beauty/a2/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                    {id:4, img:'assets/images/category/beauty/a2/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:5, img:'assets/images/category/beauty/a2/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                ]
             },
            {id:3,img:"assets/images/category/beauty/a3.png",name:"Mehendi Artist",
            sub1:[{id:1, img:'assets/images/category/beauty/a3/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:2, img:'assets/images/category/beauty/a3/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                  {id:3, img:'assets/images/category/beauty/a3/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                  {id:4, img:'assets/images/category/beauty/a3/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:5, img:'assets/images/category/beauty/a3/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                ]
            },
            {id:4,img:"assets/images/category/beauty/a4.png",name:"Unisex Parlor",
            sub1:[{id:1, img:'assets/images/category/beauty/a4/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:2, img:'assets/images/category/beauty/a4/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                  {id:3, img:'assets/images/category/beauty/a4/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                  {id:4, img:'assets/images/category/beauty/a4/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:5, img:'assets/images/category/beauty/a4/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                ]
            },
            {id:5,img:"assets/images/category/beauty/a5.png",name:"Ladies Beauty Parlor",
            sub1:[{id:1, img:'assets/images/category/beauty/a5/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:2, img:'assets/images/category/beauty/a5/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                  {id:3, img:'assets/images/category/beauty/a5/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                  {id:4, img:'assets/images/category/beauty/a5/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:5, img:'assets/images/category/beauty/a5/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                ]
            },
            {id:6,img:"assets/images/category/beauty/a6.png",name:"Beauticians",
            sub1:[{id:1, img:'assets/images/category/beauty/a6/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:2, img:'assets/images/category/beauty/a6/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                  {id:3, img:'assets/images/category/beauty/a6/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                  {id:4, img:'assets/images/category/beauty/a6/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:5, img:'assets/images/category/beauty/a6/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                ]
            },
            {id:7,img:"assets/images/category/beauty/a7.png",name:"gents salon",
            sub1:[{id:1, img:'assets/images/category/beauty/a7/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:2, img:'assets/images/category/beauty/a7/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                  {id:3, img:'assets/images/category/beauty/a7/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                  {id:4, img:'assets/images/category/beauty/a7/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:5, img:'assets/images/category/beauty/a7/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                ]
            },
            {id:8,img:"assets/images/category/beauty/a8.png",name:"Beauty Care Products",
            sub1:[{id:1, img:'assets/images/category/beauty/a8/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:2, img:'assets/images/category/beauty/a8/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                  {id:3, img:'assets/images/category/beauty/a8/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                  {id:4, img:'assets/images/category/beauty/a8/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:5, img:'assets/images/category/beauty/a8/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                ]
            },
            {id:9,img:"assets/images/category/beauty/a9.png",name:"Fashion Accessories",
            sub1:[{id:1, img:'assets/images/category/beauty/a9/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:2, img:'assets/images/category/beauty/a9/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                  {id:3, img:'assets/images/category/beauty/a9/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                  {id:4, img:'assets/images/category/beauty/a9/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:5, img:'assets/images/category/beauty/a9/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                ]
            },
          ]
     },
    {id: 2, img:"assets/images/category/automobile.png", name: "Automobile",
     sub : [{id:1,img:"assets/images/category/automobile/at1.png",name:"Automobile Dealers",
            sub1:[{id:1, img:'assets/images/category/automobile/a1/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:2, img:'assets/images/category/automobile/a1/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                  {id:3, img:'assets/images/category/automobile/a1/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                  {id:4, img:'assets/images/category/automobile/a1/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:5, img:'assets/images/category/automobile/a1/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                ]
            },
            {id:2,img:"assets/images/category/automobile/at2.png",name:"Car Cleaning Services",
            sub1:[{id:1, img:'assets/images/category/automobile/a2/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:2, img:'assets/images/category/automobile/a2/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                  {id:3, img:'assets/images/category/automobile/a2/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                  {id:4, img:'assets/images/category/automobile/a2/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:5, img:'assets/images/category/automobile/a2/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                ]
            },
            {id:3,img:"assets/images/category/automobile/at3.png",name:"Two Wheeler Spare Parts",
            sub1:[{id:1, img:'assets/images/category/automobile/a3/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:2, img:'assets/images/category/automobile/a3/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                  {id:3, img:'assets/images/category/automobile/a3/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                  {id:4, img:'assets/images/category/automobile/a3/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:5, img:'assets/images/category/automobile/a3/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                ]
            },
            {id:4,img:"assets/images/category/automobile/at4.png",name:"Car Sun Control Film Dealers",
            sub1:[{id:1, img:'assets/images/category/automobile/a4/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:2, img:'assets/images/category/automobile/a4/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                  {id:3, img:'assets/images/category/automobile/a4/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                  {id:4, img:'assets/images/category/automobile/a4/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:5, img:'assets/images/category/automobile/a4/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                ]
            },
            {id:5,img:"assets/images/category/automobile/at5.png",name:"Car Polishing Services",
            sub1:[{id:1, img:'assets/images/category/automobile/a5/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:2, img:'assets/images/category/automobile/a5/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                  {id:3, img:'assets/images/category/automobile/a5/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                  {id:4, img:'assets/images/category/automobile/a5/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:5, img:'assets/images/category/automobile/a5/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                ]
            }, 
            {id:6,img:"assets/images/category/automobile/at6.png",name:"Automobile Service Station",
            sub1:[{id:1, img:'assets/images/category/automobile/a6/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:2, img:'assets/images/category/automobile/a6/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                  {id:3, img:'assets/images/category/automobile/a6/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                  {id:4, img:'assets/images/category/automobile/a6/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:5, img:'assets/images/category/automobile/a6/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                ]
            },
            {id:7,img:"assets/images/category/automobile/at7.png",name:"Car Painting",
            sub1:[{id:1, img:'assets/images/category/automobile/a7/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:2, img:'assets/images/category/automobile/a7/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                  {id:3, img:'assets/images/category/automobile/a7/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                  {id:4, img:'assets/images/category/automobile/a7/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:5, img:'assets/images/category/automobile/a7/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                ]
            },
            {id:8,img:"assets/images/category/automobile/at8.png",name:"Secondhand Cars Consultants",
            sub1:[{id:1, img:'assets/images/category/automobile/a8/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:2, img:'assets/images/category/automobile/a8/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                  {id:3, img:'assets/images/category/automobile/a8/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                  {id:4, img:'assets/images/category/automobile/a8/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:5, img:'assets/images/category/automobile/a8/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                ]
            },
            {id:9,img:"assets/images/category/automobile/at9.png",name:"Car Tyre Dealers",
            sub1:[{id:1, img:'assets/images/category/automobile/a9/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:2, img:'assets/images/category/automobile/a9/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                  {id:3, img:'assets/images/category/automobile/a9/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                  {id:4, img:'assets/images/category/automobile/a9/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:5, img:'assets/images/category/automobile/a9/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                ]
            },
            {id:10,img:"assets/images/category/automobile/at10.png",name:"Car Accessories",
            sub1:[{id:1, img:'assets/images/category/automobile/a10/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:2, img:'assets/images/category/automobile/a10/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                  {id:3, img:'assets/images/category/automobile/a10/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                  {id:4, img:'assets/images/category/automobile/a10/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:5, img:'assets/images/category/automobile/a10/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                ]
            },
            {id:11,img:"assets/images/category/automobile/at11.png",name:"Car Service Station",
            sub1:[{id:1, img:'assets/images/category/automobile/a11/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:2, img:'assets/images/category/automobile/a11/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                  {id:3, img:'assets/images/category/automobile/a11/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                  {id:4, img:'assets/images/category/automobile/a11/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:5, img:'assets/images/category/automobile/a11/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                ]
            },
            {id:12,img:"assets/images/category/automobile/at12.png",name:"Tractor and Trolley Equipment and Parts",
            sub1:[{id:1, img:'assets/images/category/automobile/a12/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:2, img:'assets/images/category/automobile/a12/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                  {id:3, img:'assets/images/category/automobile/a12/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                  {id:4, img:'assets/images/category/automobile/a12/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  {id:5, img:'assets/images/category/automobile/a12/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                ]
            },
          ]
    },
    {id: 3, img:"assets/images/category/doctor.png", name: "Doctor" ,
      sub : [{id:1,img:"assets/images/category/doctor/d1.png",name:"Homeopathic Doctors",
              sub1:[{id:1, img:'assets/images/category/doctor/a1/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:2, img:'assets/images/category/doctor/a1/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                    {id:3, img:'assets/images/category/doctor/a1/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                    {id:4, img:'assets/images/category/doctor/a1/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:5, img:'assets/images/category/doctor/a1/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  ]
              },
              {id:2,img:"assets/images/category/doctor/d2.png",name:"Hearing Aid Center",
              sub1:[{id:1, img:'assets/images/category/doctor/a2/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:2, img:'assets/images/category/doctor/a2/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                    {id:3, img:'assets/images/category/doctor/a2/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                    {id:4, img:'assets/images/category/doctor/a2/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:5, img:'assets/images/category/doctor/a2/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  ]
              },
              {id:3,img:"assets/images/category/doctor/d3.png",name:"Acupuncture Doctors",
              sub1:[{id:1, img:'assets/images/category/doctor/a3/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:2, img:'assets/images/category/doctor/a3/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                    {id:3, img:'assets/images/category/doctor/a3/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                    {id:4, img:'assets/images/category/doctor/a3/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:5, img:'assets/images/category/doctor/a3/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  ]
              },
              {id:4,img:"assets/images/category/doctor/d4.png",name:"Skin Specialist Doctors",
              sub1:[{id:1, img:'assets/images/category/doctor/a4/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:2, img:'assets/images/category/doctor/a4/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                    {id:3, img:'assets/images/category/doctor/a4/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                    {id:4, img:'assets/images/category/doctor/a4/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:5, img:'assets/images/category/doctor/a4/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  ]
              },
              {id:5,img:"assets/images/category/doctor/d5.png",name:"Pediatricians",
              sub1:[{id:1, img:'assets/images/category/doctor/a5/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:2, img:'assets/images/category/doctor/a5/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                    {id:3, img:'assets/images/category/doctor/a5/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                    {id:4, img:'assets/images/category/doctor/a5/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:5, img:'assets/images/category/doctor/a5/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  ]
              }, 
              {id:6,img:"assets/images/category/doctor/d6.png",name:"Neurologists",
              sub1:[{id:1, img:'assets/images/category/doctor/a6/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:2, img:'assets/images/category/doctor/a6/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                    {id:3, img:'assets/images/category/doctor/a6/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                    {id:4, img:'assets/images/category/doctor/a6/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:5, img:'assets/images/category/doctor/a6/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  ]
              },
              {id:7,img:"assets/images/category/doctor/d7.png",name:"gynaecologist & obstetrician Doctors",
              sub1:[{id:1, img:'assets/images/category/doctor/a7/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:2, img:'assets/images/category/doctor/a7/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                    {id:3, img:'assets/images/category/doctor/a7/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                    {id:4, img:'assets/images/category/doctor/a7/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:5, img:'assets/images/category/doctor/a7/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  ]
              },
              {id:8,img:"assets/images/category/doctor/d8.png",name:"Orthopaedic Surgeons",
              sub1:[{id:1, img:'assets/images/category/doctor/a8/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:2, img:'assets/images/category/doctor/a8/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                    {id:3, img:'assets/images/category/doctor/a8/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                    {id:4, img:'assets/images/category/doctor/a8/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:5, img:'assets/images/category/doctor/a8/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  ]
              },
              {id:9,img:"assets/images/category/doctor/d9.png",name:"General Physician Doctors",
              sub1:[{id:1, img:'assets/images/category/doctor/a9/aa1.jpg', name :"Dhingra Health Clinic ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:2, img:'assets/images/category/doctor/a9/aa2.jpg', name:"Dr. Mohammed Faizan ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                    {id:3, img:'assets/images/category/doctor/a9/aa3.jpg', name:"Grace Ortho Clinic ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                    {id:4, img:'assets/images/category/doctor/a9/aa4.jpg', name:"Dr Madan Kapre ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:5, img:'assets/images/category/doctor/a9/aa5.jpg', name:"Masand Nursing Home", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  ]
              },
              {id:10,img:"assets/images/category/doctor/d10.png",name:"Dermatologists",
              sub1:[{id:1, img:'assets/images/category/doctor/a10/aa1.jpg', name :"Dhingra Health Clinic ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:2, img:'assets/images/category/doctor/a10/aa2.jpg', name:"Dr. Mohammed Faizan ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                    {id:3, img:'assets/images/category/doctor/a10/aa3.jpg', name:"Grace Ortho Clinic ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                    {id:4, img:'assets/images/category/doctor/a10/aa4.jpg', name:"Dr Madan Kapre  ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:5, img:'assets/images/category/doctor/a10/aa5.jpg', name:"Masand Nursing Home ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  ]
              },
              {id:11,img:"assets/images/category/doctor/d11.png",name:"Dietitians",
              sub1:[{id:1, img:'assets/images/category/doctor/a11/aa1.jpg', name :"Dhingra Health Clinic", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:2, img:'assets/images/category/doctor/a11/aa2.jpg', name:"Dr. Mohammed Faizan  ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                    {id:3, img:'assets/images/category/doctor/a11/aa3.jpg', name:"Grace Ortho Clinic ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                    {id:4, img:'assets/images/category/doctor/a11/aa4.jpg', name:"Dr Madan Kapre ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:5, img:'assets/images/category/doctor/a11/aa5.jpg', name:"Masand Nursing Home  ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  ]
              },
              {id:12,img:"assets/images/category/doctor/d12.png",name:"Pathology Labs",
              sub1:[{id:1, img:'assets/images/category/doctor/a12/aa1.jpg', name :"Dhingra Health Clinic", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:2, img:'assets/images/category/doctor/a12/aa2.jpg', name:"Dr. Mohammed Faizan", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                    {id:3, img:'assets/images/category/doctor/a12/aa3.jpg', name:"Grace Ortho Clinic  ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                    {id:4, img:'assets/images/category/doctor/a12/aa4.jpg', name:"Dr Madan Kapre", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:5, img:'assets/images/category/doctor/a12/aa5.jpg', name:"Masand Nursing Home ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  ]
              },
            ]
      },
      {id: 4, img:"assets/images/category/real-estate.png", name: "Real Estate" ,
      sub : [{id:1,img:"assets/images/category/real estate/r1.png",name:"Hostel",
              sub1:[{id:1, img:'assets/images/category/real estate/a1/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:2, img:'assets/images/category/real estate/a1/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                    {id:3, img:'assets/images/category/real estate/a1/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                    {id:4, img:'assets/images/category/real estate/a1/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:5, img:'assets/images/category/real estate/a1/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  ]
              },
              {id:2,img:"assets/images/category/real estate/r2.png",name:"Farm House",
              sub1:[{id:1, img:'assets/images/category/real estate/a2/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:2, img:'assets/images/category/real estate/a2/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                    {id:3, img:'assets/images/category/real estate/a2/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                    {id:4, img:'assets/images/category/real estate/a2/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:5, img:'assets/images/category/real estate/a2/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  ]
              },
              {id:3,img:"assets/images/category/real estate/r3.png",name:"Land Developers",
              sub1:[{id:1, img:'assets/images/category/real estate/a3/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:2, img:'assets/images/category/real estate/a3/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                    {id:3, img:'assets/images/category/real estate/a3/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                    {id:4, img:'assets/images/category/real estate/a3/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:5, img:'assets/images/category/real estate/a3/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  ]
              },
              {id:4,img:"assets/images/category/real estate/r4.png",name:"Builders and Developers",
              sub1:[{id:1, img:'assets/images/category/real estate/a4/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:2, img:'assets/images/category/real estate/a4/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                    {id:3, img:'assets/images/category/real estate/a4/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                    {id:4, img:'assets/images/category/real estate/a4/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:5, img:'assets/images/category/real estate/a4/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  ]
              },
              {id:5,img:"assets/images/category/real estate/r5.png",name:"Godowns",
              sub1:[{id:1, img:'assets/images/category/real estate/a5/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:2, img:'assets/images/category/real estate/a5/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                    {id:3, img:'assets/images/category/real estate/a5/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                    {id:4, img:'assets/images/category/real estate/a5/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:5, img:'assets/images/category/real estate/a5/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  ]
              }, 
              {id:6,img:"assets/images/category/real estate/r6.png",name:"Warehouses",
              sub1:[{id:1, img:'assets/images/category/real estate/a6/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:2, img:'assets/images/category/real estate/a6/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                    {id:3, img:'assets/images/category/real estate/a6/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                    {id:4, img:'assets/images/category/real estate/a6/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:5, img:'assets/images/category/real estate/a6/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  ]
              },
              {id:7,img:"assets/images/category/real estate/r7.png",name:"Construction Companies",
              sub1:[{id:1, img:'assets/images/category/real estate/a7/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:2, img:'assets/images/category/real estate/a7/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                    {id:3, img:'assets/images/category/real estate/a7/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                    {id:4, img:'assets/images/category/real estate/a7/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:5, img:'assets/images/category/real estate/a7/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  ]
              },
              {id:8,img:"assets/images/category/real estate/r8.png",name:"Real Estate Developers",
              sub1:[{id:1, img:'assets/images/category/real estate/a8/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:2, img:'assets/images/category/real estate/a8/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                    {id:3, img:'assets/images/category/real estate/a8/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                    {id:4, img:'assets/images/category/real estate/a8/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:5, img:'assets/images/category/real estate/a8/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  ]
              },
              {id:9,img:"assets/images/category/real estate/r9.png",name:"Tours & Travels",
              sub1:[{id:1, img:'assets/images/category/real estate/a1/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:2, img:'assets/images/category/real estate/a1/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                    {id:3, img:'assets/images/category/real estate/a1/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                    {id:4, img:'assets/images/category/real estate/a1/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:5, img:'assets/images/category/real estate/a1/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  ]
              },
              {id:10,img:"assets/images/category/real estate/r10.png",name:"Real Estate Agents",
              sub1:[{id:1, img:'assets/images/category/real estate/a1/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:2, img:'assets/images/category/real estate/a1/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                    {id:3, img:'assets/images/category/real estate/a1/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                    {id:4, img:'assets/images/category/real estate/a1/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:5, img:'assets/images/category/real estate/a1/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  ]
              },
              {id:11,img:"assets/images/category/real estate/r11.png",name:"Commercial Property Dealers",
              sub1:[{id:1, img:'assets/images/category/real estate/a1/aa1.jpg', name :"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:2, img:'assets/images/category/real estate/a1/aa2.jpg', name:"Paathshala Salon Academy ", phone:"9309729615",address:" Rathsaptami Apartments, Plot No. T-10,…",review:"2review"},
                    {id:3, img:'assets/images/category/real estate/a1/aa3.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"0 review"},
                    {id:4, img:'assets/images/category/real estate/a1/aa4.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                    {id:5, img:'assets/images/category/real estate/a1/aa5.jpg', name:"Attitude Unisex Salon ", phone:"  0712-6610199, 9922389036, 9096015645.",address:"Shop No.5, Vaibhavlaxmi Apartment, Laxmi…",review:"1 review"},
                  ]
              },
 
            ]
      },
      {id: 5, img:"assets/images/category/IT.png", name: "IT" ,
      sub : [{id:1,img:"assets/images/category/IT/it1.png",name:"Laptop Sales and Services",
              sub1:[{id:1, img:'assets/images/category/IT/a1/aa1.jpg', name :"IT Way Technologies - Web, Software, App Development", phone:"  9970773211..",address:" Mangal Murti Chawk,",review:"1 review"},
                    {id:2, img:'assets/images/category/IT/a1/aa2.jpg', name:"Aaditya Internet Cafe ", phone:" 9763714087.",address:" Hingna T-Point Square, Hingna Road,",review:"2review"},
                    {id:3, img:'assets/images/category/IT/a1/aa3.jpg', name:"Adarsh Command Service Centre ", phone:"  8888334786.",address:"Snehnagar Mauda Near Bus Stop,",review:"0 review"},
                    {id:4, img:'assets/images/category/IT/a1/aa4.jpg', name:"Adi N Tech ", phone:"  8552931134.",address:" Mangalmurti square",review:"1 review"},
                    {id:5, img:'assets/images/category/IT/a1/aa5.jpg', name:"Aryan Internet Cafe", phone:"  7709453001.",address:" Shop No. 5, Rachna Zeel…",review:"1 review"},
                  ]
            },
            {id:2,img:"assets/images/category/IT/it2.png",name:"Computer Anti Virus",
                  sub1:[{id:1, img:'assets/images/category/IT/a2/aa1.jpg', name :"IT Way Technologies - Web, Software, App Development", phone:"  9970773211..",address:" Mangal Murti Chawk,",review:"1 review"},
                        {id:2, img:'assets/images/category/IT/a2/aa2.jpg', name:"Aaditya Internet Cafe ", phone:" 9763714087.",address:" Hingna T-Point Square, Hingna Road,",review:"2review"},
                        {id:3, img:'assets/images/category/IT/a2/aa3.jpg', name:"Adarsh Command Service Centre ", phone:"  8888334786.",address:"Snehnagar Mauda Near Bus Stop,",review:"0 review"},
                        {id:4, img:'assets/images/category/IT/a2/aa4.jpg', name:"Adi N Tech ", phone:"  8552931134.",address:" Mangalmurti square",review:"1 review"},
                        {id:5, img:'assets/images/category/IT/a2/aa5.jpg', name:"Aryan Internet Cafe", phone:"  7709453001.",address:" Shop No. 5, Rachna Zeel…",review:"1 review"},
                      ]
            },
            {id:3,img:"assets/images/category/IT/it3.png",name:"Cyber Cafe",
                  sub1:[{id:1, img:'assets/images/category/IT/a3/aa1.jpg', name :"IT Way Technologies - Web, Software, App Development", phone:"  9970773211..",address:" Mangal Murti Chawk,",review:"1 review"},
                        {id:2, img:'assets/images/category/IT/a3/aa2.jpg', name:"Aaditya Internet Cafe ", phone:" 9763714087.",address:" Hingna T-Point Square, Hingna Road,",review:"2review"},
                        {id:3, img:'assets/images/category/IT/a3/aa3.jpg', name:"Adarsh Command Service Centre ", phone:"  8888334786.",address:"Snehnagar Mauda Near Bus Stop,",review:"0 review"},
                        {id:4, img:'assets/images/category/IT/a3/aa4.jpg', name:"Adi N Tech ", phone:"  8552931134.",address:" Mangalmurti square",review:"1 review"},
                        {id:5, img:'assets/images/category/IT/a3/aa5.jpg', name:"Aryan Internet Cafe", phone:"  7709453001.",address:" Shop No. 5, Rachna Zeel…",review:"1 review"},
                      ]
            },
            {id:4,img:"assets/images/category/IT/it4.png",name:"Computer Software Dealers",
                  sub1:[{id:1, img:'assets/images/category/IT/a4/aa1.jpg', name :"IT Way Technologies - Web, Software, App Development", phone:"  9970773211..",address:" Mangal Murti Chawk,",review:"1 review"},
                        {id:2, img:'assets/images/category/IT/a4/aa2.jpg', name:"Aaditya Internet Cafe ", phone:" 9763714087.",address:" Hingna T-Point Square, Hingna Road,",review:"2review"},
                        {id:3, img:'assets/images/category/IT/a4/aa3.jpg', name:"Adarsh Command Service Centre ", phone:"  8888334786.",address:"Snehnagar Mauda Near Bus Stop,",review:"0 review"},
                        {id:4, img:'assets/images/category/IT/a4/aa4.jpg', name:"Adi N Tech ", phone:"  8552931134.",address:" Mangalmurti square",review:"1 review"},
                        {id:5, img:'assets/images/category/IT/a4/aa5.jpg', name:"Aryan Internet Cafe", phone:"  7709453001.",address:" Shop No. 5, Rachna Zeel…",review:"1 review"},
                      ]
            },
            {id:5,img:"assets/images/category/IT/it5.png",name:"Software Company",
                  sub1:[{id:1, img:'assets/images/category/IT/a5/aa1.jpg', name :"IT Way Technologies - Web, Software, App Development", phone:"  9970773211..",address:" Mangal Murti Chawk,",review:"1 review"},
                        {id:2, img:'assets/images/category/IT/a5/aa2.jpg', name:"Aaditya Internet Cafe ", phone:" 9763714087.",address:" Hingna T-Point Square, Hingna Road,",review:"2review"},
                        {id:3, img:'assets/images/category/IT/a5/aa3.jpg', name:"Adarsh Command Service Centre ", phone:"  8888334786.",address:"Snehnagar Mauda Near Bus Stop,",review:"0 review"},
                        {id:4, img:'assets/images/category/IT/a5/aa4.jpg', name:"Adi N Tech ", phone:"  8552931134.",address:" Mangalmurti square",review:"1 review"},
                        {id:5, img:'assets/images/category/IT/a5/aa5.jpg', name:"Aryan Internet Cafe", phone:"  7709453001.",address:" Shop No. 5, Rachna Zeel…",review:"1 review"},
                      ]
            }, 
            {id:6,img:"assets/images/category/IT/it6.png",name:"Digital Marketing Comapny",
                  sub1:[{id:1, img:'assets/images/category/IT/a6/aa1.jpg', name :"IT Way Technologies - Web, Software, App Development", phone:"  9970773211..",address:" Mangal Murti Chawk,",review:"1 review"},
                        {id:2, img:'assets/images/category/IT/a6/aa2.jpg', name:"Aaditya Internet Cafe ", phone:" 9763714087.",address:" Hingna T-Point Square, Hingna Road,",review:"2review"},
                        {id:3, img:'assets/images/category/IT/a6/aa3.jpg', name:"Adarsh Command Service Centre ", phone:"  8888334786.",address:"Snehnagar Mauda Near Bus Stop,",review:"0 review"},
                        {id:4, img:'assets/images/category/IT/a6/aa4.jpg', name:"Adi N Tech ", phone:"  8552931134.",address:" Mangalmurti square",review:"1 review"},
                        {id:5, img:'assets/images/category/IT/a6/aa5.jpg', name:"Aryan Internet Cafe", phone:"  7709453001.",address:" Shop No. 5, Rachna Zeel…",review:"1 review"},
                      ]  
            },
            {id:7,img:"assets/images/category/IT/it7.png",name:"Computer Repair & Services",
                  sub1:[{id:1, img:'assets/images/category/IT/a7/aa1.jpg', name :"IT Way Technologies - Web, Software, App Development", phone:"  9970773211..",address:" Mangal Murti Chawk,",review:"1 review"},
                        {id:2, img:'assets/images/category/IT/a7/aa2.jpg', name:"Aaditya Internet Cafe ", phone:" 9763714087.",address:" Hingna T-Point Square, Hingna Road,",review:"2review"},
                        {id:3, img:'assets/images/category/IT/a7/aa3.jpg', name:"Adarsh Command Service Centre ", phone:"  8888334786.",address:"Snehnagar Mauda Near Bus Stop,",review:"0 review"},
                        {id:4, img:'assets/images/category/IT/a7/aa4.jpg', name:"Adi N Tech ", phone:"  8552931134.",address:" Mangalmurti square",review:"1 review"},
                        {id:5, img:'assets/images/category/IT/a7/aa5.jpg', name:"Aryan Internet Cafe", phone:"  7709453001.",address:" Shop No. 5, Rachna Zeel…",review:"1 review"},
                      ] 
            },
            {id:8,img:"assets/images/category/IT/it8.png",name:"LAN Services",
                  sub1:[{id:1, img:'assets/images/category/IT/a8/aa1.jpg', name :"IT Way Technologies - Web, Software, App Development", phone:"  9970773211..",address:" Mangal Murti Chawk,",review:"1 review"},
                        {id:2, img:'assets/images/category/IT/a8/aa2.jpg', name:"Aaditya Internet Cafe ", phone:" 9763714087.",address:" Hingna T-Point Square, Hingna Road,",review:"2review"},
                        {id:3, img:'assets/images/category/IT/a8/aa3.jpg', name:"Adarsh Command Service Centre ", phone:"  8888334786.",address:"Snehnagar Mauda Near Bus Stop,",review:"0 review"},
                        {id:4, img:'assets/images/category/IT/a8/aa4.jpg', name:"Adi N Tech ", phone:"  8552931134.",address:" Mangalmurti square",review:"1 review"},
                        {id:5, img:'assets/images/category/IT/a8/aa5.jpg', name:"Aryan Internet Cafe", phone:"  7709453001.",address:" Shop No. 5, Rachna Zeel…",review:"1 review"},
                      ] 
            },
          ]
      },
      {id: 6, img:"assets/images/category/pets.png", name: "Pets" ,
      sub : [{id:1,img:"assets/images/category/pets/p1.png",name:"Pet Shops For Dogs",
            sub1:[{id:1, img:'assets/images/category/pets/a1/aa1.jpg', name :"IT Way Technologies - Web, Software, App Development", phone:"  9970773211..",address:" Mangal Murti Chawk,",review:"1 review"},
                  {id:2, img:'assets/images/category/pets/a1/aa2.jpg', name:"Aaditya Internet Cafe ", phone:" 9763714087.",address:" Hingna T-Point Square, Hingna Road,",review:"2review"},
                  {id:3, img:'assets/images/category/pets/a1/aa3.jpg', name:"Adarsh Command Service Centre ", phone:"  8888334786.",address:"Snehnagar Mauda Near Bus Stop,",review:"0 review"},
                  {id:4, img:'assets/images/category/pets/a1/aa4.jpg', name:"Adi N Tech ", phone:"  8552931134.",address:" Mangalmurti square",review:"1 review"},
                  {id:5, img:'assets/images/category/pets/a1/aa5.jpg', name:"Aryan Internet Cafe", phone:"  7709453001.",address:" Shop No. 5, Rachna Zeel…",review:"1 review"},
                ] 
            },
              {id:2,img:"assets/images/category/pets/p2.png",name:"Fish Aquariums",
              sub1:[{id:1, img:'assets/images/category/pets/a2/aa1.jpg', name :"IT Way Technologies - Web, Software, App Development", phone:"  9970773211..",address:" Mangal Murti Chawk,",review:"1 review"},
                    {id:2, img:'assets/images/category/pets/a2/aa2.jpg', name:"Aaditya Internet Cafe ", phone:" 9763714087.",address:" Hingna T-Point Square, Hingna Road,",review:"2review"},
                    {id:3, img:'assets/images/category/pets/a2/aa3.jpg', name:"Adarsh Command Service Centre ", phone:"  8888334786.",address:"Snehnagar Mauda Near Bus Stop,",review:"0 review"},
                    {id:4, img:'assets/images/category/pets/a2/aa4.jpg', name:"Adi N Tech ", phone:"  8552931134.",address:" Mangalmurti square",review:"1 review"},
                    {id:5, img:'assets/images/category/pets/a2/aa5.jpg', name:"Aryan Internet Cafe", phone:"  7709453001.",address:" Shop No. 5, Rachna Zeel…",review:"1 review"},
                  ] 
              },
              {id:3,img:"assets/images/category/pets/p3.png",name:"Pets Accessories",
                    sub1:[{id:1, img:'assets/images/category/pets/a3/aa1.jpg', name :"IT Way Technologies - Web, Software, App Development", phone:"  9970773211..",address:" Mangal Murti Chawk,",review:"1 review"},
                          {id:2, img:'assets/images/category/pets/a3/aa2.jpg', name:"Aaditya Internet Cafe ", phone:" 9763714087.",address:" Hingna T-Point Square, Hingna Road,",review:"2review"},
                          {id:3, img:'assets/images/category/pets/a3/aa3.jpg', name:"Adarsh Command Service Centre ", phone:"  8888334786.",address:"Snehnagar Mauda Near Bus Stop,",review:"0 review"},
                          {id:4, img:'assets/images/category/pets/a3/aa4.jpg', name:"Adi N Tech ", phone:"  8552931134.",address:" Mangalmurti square",review:"1 review"},
                          {id:5, img:'assets/images/category/pets/a3/aa5.jpg', name:"Aryan Internet Cafe", phone:"  7709453001.",address:" Shop No. 5, Rachna Zeel…",review:"1 review"},
                        ] 
              },
              {id:4,img:"assets/images/category/pets/p4.png",name:"Pets Shops",
                    sub1:[{id:1, img:'assets/images/category/pets/a4/aa1.jpg', name :"IT Way Technologies - Web, Software, App Development", phone:"  9970773211..",address:" Mangal Murti Chawk,",review:"1 review"},
                          {id:2, img:'assets/images/category/pets/a4/aa2.jpg', name:"Aaditya Internet Cafe ", phone:" 9763714087.",address:" Hingna T-Point Square, Hingna Road,",review:"2review"},
                          {id:3, img:'assets/images/category/pets/a4/aa3.jpg', name:"Adarsh Command Service Centre ", phone:"  8888334786.",address:"Snehnagar Mauda Near Bus Stop,",review:"0 review"},
                          {id:4, img:'assets/images/category/pets/a4/aa4.jpg', name:"Adi N Tech ", phone:"  8552931134.",address:" Mangalmurti square",review:"1 review"},
                          {id:5, img:'assets/images/category/pets/a4/aa5.jpg', name:"Aryan Internet Cafe", phone:"  7709453001.",address:" Shop No. 5, Rachna Zeel…",review:"1 review"},
                        ] 
              },
              {id:5,img:"assets/images/category/pets/p5.png",name:"Aquarium Accessories",
                    sub1:[{id:1, img:'assets/images/category/pets/a5/aa1.jpg', name :"IT Way Technologies - Web, Software, App Development", phone:"  9970773211..",address:" Mangal Murti Chawk,",review:"1 review"},
                          {id:2, img:'assets/images/category/pets/a5/aa2.jpg', name:"Aaditya Internet Cafe ", phone:" 9763714087.",address:" Hingna T-Point Square, Hingna Road,",review:"2review"},
                          {id:3, img:'assets/images/category/pets/a5/aa3.jpg', name:"Adarsh Command Service Centre ", phone:"  8888334786.",address:"Snehnagar Mauda Near Bus Stop,",review:"0 review"},
                          {id:4, img:'assets/images/category/pets/a5/aa4.jpg', name:"Adi N Tech ", phone:"  8552931134.",address:" Mangalmurti square",review:"1 review"},
                          {id:5, img:'assets/images/category/pets/a5/aa5.jpg', name:"Aryan Internet Cafe", phone:"  7709453001.",address:" Shop No. 5, Rachna Zeel…",review:"1 review"},
                        ] 
              }, 
              {id:6,img:"assets/images/category/pets/p6.png",name:"Pet Care",
                    sub1:[{id:1, img:'assets/images/category/pets/a6/aa1.jpg', name :"IT Way Technologies - Web, Software, App Development", phone:"  9970773211..",address:" Mangal Murti Chawk,",review:"1 review"},
                          {id:2, img:'assets/images/category/pets/a6/aa2.jpg', name:"Aaditya Internet Cafe ", phone:" 9763714087.",address:" Hingna T-Point Square, Hingna Road,",review:"2review"},
                          {id:3, img:'assets/images/category/pets/a6/aa3.jpg', name:"Adarsh Command Service Centre ", phone:"  8888334786.",address:"Snehnagar Mauda Near Bus Stop,",review:"0 review"},
                          {id:4, img:'assets/images/category/pets/a6/aa4.jpg', name:"Adi N Tech ", phone:"  8552931134.",address:" Mangalmurti square",review:"1 review"},
                          {id:5, img:'assets/images/category/pets/a6/aa5.jpg', name:"Aryan Internet Cafe", phone:"  7709453001.",address:" Shop No. 5, Rachna Zeel…",review:"1 review"},
                        ] 
              }, 
              {id:7,img:"assets/images/category/pets/p7.png",name:"Pets Food",
                    sub1:[{id:1, img:'assets/images/category/pets/a7/aa1.jpg', name :"IT Way Technologies - Web, Software, App Development", phone:"  9970773211..",address:" Mangal Murti Chawk,",review:"1 review"},
                          {id:2, img:'assets/images/category/pets/a7/aa2.jpg', name:"Aaditya Internet Cafe ", phone:" 9763714087.",address:" Hingna T-Point Square, Hingna Road,",review:"2review"},
                          {id:3, img:'assets/images/category/pets/a7/aa3.jpg', name:"Adarsh Command Service Centre ", phone:"  8888334786.",address:"Snehnagar Mauda Near Bus Stop,",review:"0 review"},
                          {id:4, img:'assets/images/category/pets/a7/aa4.jpg', name:"Adi N Tech ", phone:"  8552931134.",address:" Mangalmurti square",review:"1 review"},
                          {id:5, img:'assets/images/category/pets/a7/aa5.jpg', name:"Aryan Internet Cafe", phone:"  7709453001.",address:" Shop No. 5, Rachna Zeel…",review:"1 review"},
                        ] 
              }, 
           
            ]
      },
      {id: 7, img:"assets/images/category/wedding.png", name: "Wedding" ,
      sub : [{id:1,img:"assets/images/category/wedding/w1.png",name:"Wedding Planners",
              sub1:[{id:1, img:'assets/images/category/wedding/a1/aa1.jpg', name :"IT Way Technologies - Web, Software, App Development", phone:"  9970773211..",address:" Mangal Murti Chawk,",review:"1 review"},
                    {id:2, img:'assets/images/category/wedding/a1/aa2.jpg', name:"Aaditya Internet Cafe ", phone:" 9763714087.",address:" Hingna T-Point Square, Hingna Road,",review:"2review"},
                    {id:3, img:'assets/images/category/wedding/a1/aa3.jpg', name:"Adarsh Command Service Centre ", phone:"  8888334786.",address:"Snehnagar Mauda Near Bus Stop,",review:"0 review"},
                    {id:4, img:'assets/images/category/wedding/a1/aa4.jpg', name:"Adi N Tech ", phone:"  8552931134.",address:" Mangalmurti square",review:"1 review"},
                    {id:5, img:'assets/images/category/wedding/a1/aa5.jpg', name:"Aryan Internet Cafe", phone:"  7709453001.",address:" Shop No. 5, Rachna Zeel…",review:"1 review"},
                  ] 
              },
              {id:2,img:"assets/images/category/wedding/w2.png",name:"Lawns",
              sub1:[{id:1, img:'assets/images/category/wedding/a2/aa1.jpg', name :"IT Way Technologies - Web, Software, App Development", phone:"  9970773211..",address:" Mangal Murti Chawk,",review:"1 review"},
                    {id:2, img:'assets/images/category/wedding/a2/aa2.jpg', name:"Aaditya Internet Cafe ", phone:" 9763714087.",address:" Hingna T-Point Square, Hingna Road,",review:"2review"},
                    {id:3, img:'assets/images/category/wedding/a2/aa3.jpg', name:"Adarsh Command Service Centre ", phone:"  8888334786.",address:"Snehnagar Mauda Near Bus Stop,",review:"0 review"},
                    {id:4, img:'assets/images/category/wedding/a2/aa4.jpg', name:"Adi N Tech ", phone:"  8552931134.",address:" Mangalmurti square",review:"1 review"},
                    {id:5, img:'assets/images/category/wedding/a2/aa5.jpg', name:"Aryan Internet Cafe", phone:"  7709453001.",address:" Shop No. 5, Rachna Zeel…",review:"1 review"},
                  ] 
              },
              {id:3,img:"assets/images/category/wedding/w3.png",name:"Wedding Halls",
              sub1:[{id:1, img:'assets/images/category/wedding/a3/aa1.jpg', name :"IT Way Technologies - Web, Software, App Development", phone:"  9970773211..",address:" Mangal Murti Chawk,",review:"1 review"},
                    {id:2, img:'assets/images/category/wedding/a3/aa2.jpg', name:"Aaditya Internet Cafe ", phone:" 9763714087.",address:" Hingna T-Point Square, Hingna Road,",review:"2review"},
                    {id:3, img:'assets/images/category/wedding/a3/aa3.jpg', name:"Adarsh Command Service Centre ", phone:"  8888334786.",address:"Snehnagar Mauda Near Bus Stop,",review:"0 review"},
                    {id:4, img:'assets/images/category/wedding/a3/aa4.jpg', name:"Adi N Tech ", phone:"  8552931134.",address:" Mangalmurti square",review:"1 review"},
                    {id:5, img:'assets/images/category/wedding/a3/aa5.jpg', name:"Aryan Internet Cafe", phone:"  7709453001.",address:" Shop No. 5, Rachna Zeel…",review:"1 review"},
                  ] 
              },
              {id:4,img:"assets/images/category/wedding/w4.png",name:"Best Banquet Halls",
              sub1:[{id:1, img:'assets/images/category/wedding/a4/aa1.jpg', name :"IT Way Technologies - Web, Software, App Development", phone:"  9970773211..",address:" Mangal Murti Chawk,",review:"1 review"},
                    {id:2, img:'assets/images/category/wedding/a4/aa2.jpg', name:"Aaditya Internet Cafe ", phone:" 9763714087.",address:" Hingna T-Point Square, Hingna Road,",review:"2review"},
                    {id:3, img:'assets/images/category/wedding/a4/aa3.jpg', name:"Adarsh Command Service Centre ", phone:"  8888334786.",address:"Snehnagar Mauda Near Bus Stop,",review:"0 review"},
                    {id:4, img:'assets/images/category/wedding/a4/aa4.jpg', name:"Adi N Tech ", phone:"  8552931134.",address:" Mangalmurti square",review:"1 review"},
                    {id:5, img:'assets/images/category/wedding/a4/aa5.jpg', name:"Aryan Internet Cafe", phone:"  7709453001.",address:" Shop No. 5, Rachna Zeel…",review:"1 review"},
                  ] 
              },
              {id:5,img:"assets/images/category/wedding/w5.png",name:"5 Star Banquet Halls",
              sub1:[{id:1, img:'assets/images/category/wedding/a5/aa1.jpg', name :"IT Way Technologies - Web, Software, App Development", phone:"  9970773211..",address:" Mangal Murti Chawk,",review:"1 review"},
                    {id:2, img:'assets/images/category/wedding/a5/aa2.jpg', name:"Aaditya Internet Cafe ", phone:" 9763714087.",address:" Hingna T-Point Square, Hingna Road,",review:"2review"},
                    {id:3, img:'assets/images/category/wedding/a5/aa3.jpg', name:"Adarsh Command Service Centre ", phone:"  8888334786.",address:"Snehnagar Mauda Near Bus Stop,",review:"0 review"},
                    {id:4, img:'assets/images/category/wedding/a5/aa4.jpg', name:"Adi N Tech ", phone:"  8552931134.",address:" Mangalmurti square",review:"1 review"},
                    {id:5, img:'assets/images/category/wedding/a5/aa5.jpg', name:"Aryan Internet Cafe", phone:"  7709453001.",address:" Shop No. 5, Rachna Zeel…",review:"1 review"},
                  ] 
              }, 
              {id:6,img:"assets/images/category/wedding/w6.png",name:"Car Decorators",
              sub1:[{id:1, img:'assets/images/category/wedding/a6/aa1.jpg', name :"IT Way Technologies - Web, Software, App Development", phone:"  9970773211..",address:" Mangal Murti Chawk,",review:"1 review"},
                    {id:2, img:'assets/images/category/wedding/a6/aa2.jpg', name:"Aaditya Internet Cafe ", phone:" 9763714087.",address:" Hingna T-Point Square, Hingna Road,",review:"2review"},
                    {id:3, img:'assets/images/category/wedding/a6/aa3.jpg', name:"Adarsh Command Service Centre ", phone:"  8888334786.",address:"Snehnagar Mauda Near Bus Stop,",review:"0 review"},
                    {id:4, img:'assets/images/category/wedding/a6/aa4.jpg', name:"Adi N Tech ", phone:"  8552931134.",address:" Mangalmurti square",review:"1 review"},
                    {id:5, img:'assets/images/category/wedding/a6/aa5.jpg', name:"Aryan Internet Cafe", phone:"  7709453001.",address:" Shop No. 5, Rachna Zeel…",review:"1 review"},
                  ] 
              },
              {id:7,img:"assets/images/category/wedding/w7.png",name:"Flower Decorators",
              sub1:[{id:1, img:'assets/images/category/wedding/a7/aa1.jpg', name :"IT Way Technologies - Web, Software, App Development", phone:"  9970773211..",address:" Mangal Murti Chawk,",review:"1 review"},
                    {id:2, img:'assets/images/category/wedding/a7/aa2.jpg', name:"Aaditya Internet Cafe ", phone:" 9763714087.",address:" Hingna T-Point Square, Hingna Road,",review:"2review"},
                    {id:3, img:'assets/images/category/wedding/a7/aa3.jpg', name:"Adarsh Command Service Centre ", phone:"  8888334786.",address:"Snehnagar Mauda Near Bus Stop,",review:"0 review"},
                    {id:4, img:'assets/images/category/wedding/a7/aa4.jpg', name:"Adi N Tech ", phone:"  8552931134.",address:" Mangalmurti square",review:"1 review"},
                    {id:5, img:'assets/images/category/wedding/a7/aa5.jpg', name:"Aryan Internet Cafe", phone:"  7709453001.",address:" Shop No. 5, Rachna Zeel…",review:"1 review"},
                  ] 
              },
              {id:8,img:"assets/images/category/wedding/w8.png",name:"Non AC Banquet Halls",
              sub1:[{id:1, img:'assets/images/category/wedding/a8/aa1.jpg', name :"IT Way Technologies - Web, Software, App Development", phone:"  9970773211..",address:" Mangal Murti Chawk,",review:"1 review"},
                    {id:2, img:'assets/images/category/wedding/a8/aa2.jpg', name:"Aaditya Internet Cafe ", phone:" 9763714087.",address:" Hingna T-Point Square, Hingna Road,",review:"2review"},
                    {id:3, img:'assets/images/category/wedding/a8/aa3.jpg', name:"Adarsh Command Service Centre ", phone:"  8888334786.",address:"Snehnagar Mauda Near Bus Stop,",review:"0 review"},
                    {id:4, img:'assets/images/category/wedding/a8/aa4.jpg', name:"Adi N Tech ", phone:"  8552931134.",address:" Mangalmurti square",review:"1 review"},
                    {id:5, img:'assets/images/category/wedding/a8/aa5.jpg', name:"Aryan Internet Cafe", phone:"  7709453001.",address:" Shop No. 5, Rachna Zeel…",review:"1 review"},
                  ] 
              },
              {id:9,img:"assets/images/category/wedding/w9.png",name:"AC Banquets Halls",
              sub1:[{id:1, img:'assets/images/category/wedding/a9/aa1.jpg', name :"IT Way Technologies - Web, Software, App Development", phone:"  9970773211..",address:" Mangal Murti Chawk,",review:"1 review"},
                    {id:2, img:'assets/images/category/wedding/a9/aa2.jpg', name:"Aaditya Internet Cafe ", phone:" 9763714087.",address:" Hingna T-Point Square, Hingna Road,",review:"2review"},
                    {id:3, img:'assets/images/category/wedding/a9/aa3.jpg', name:"Adarsh Command Service Centre ", phone:"  8888334786.",address:"Snehnagar Mauda Near Bus Stop,",review:"0 review"},
                    {id:4, img:'assets/images/category/wedding/a9/aa4.jpg', name:"Adi N Tech ", phone:"  8552931134.",address:" Mangalmurti square",review:"1 review"},
                    {id:5, img:'assets/images/category/wedding/a9/aa5.jpg', name:"Aryan Internet Cafe", phone:"  7709453001.",address:" Shop No. 5, Rachna Zeel…",review:"1 review"},
                  ] 
              },
              {id:10,img:"assets/images/category/wedding/w10.png",name:"Wedding Card Printers",
              sub1:[{id:1, img:'assets/images/category/wedding/a10/aa1.jpg', name :"IT Way Technologies - Web, Software, App Development", phone:"  9970773211..",address:" Mangal Murti Chawk,",review:"1 review"},
                    {id:2, img:'assets/images/category/wedding/a10/aa2.jpg', name:"Aaditya Internet Cafe ", phone:" 9763714087.",address:" Hingna T-Point Square, Hingna Road,",review:"2review"},
                    {id:3, img:'assets/images/category/wedding/a10/aa3.jpg', name:"Adarsh Command Service Centre ", phone:"  8888334786.",address:"Snehnagar Mauda Near Bus Stop,",review:"0 review"},
                    {id:4, img:'assets/images/category/wedding/a10/aa4.jpg', name:"Adi N Tech ", phone:"  8552931134.",address:" Mangalmurti square",review:"1 review"},
                    {id:5, img:'assets/images/category/wedding/a10/aa5.jpg', name:"Aryan Internet Cafe", phone:"  7709453001.",address:" Shop No. 5, Rachna Zeel…",review:"1 review"},
                  ] 
              },
              {id:11,img:"assets/images/category/wedding/w11.png",name:"Wedding Bands",
              sub1:[{id:1, img:'assets/images/category/wedding/a10/aa1.jpg', name :"IT Way Technologies - Web, Software, App Development", phone:"  9970773211..",address:" Mangal Murti Chawk,",review:"1 review"},
                    {id:2, img:'assets/images/category/wedding/a10/aa2.jpg', name:"Aaditya Internet Cafe ", phone:" 9763714087.",address:" Hingna T-Point Square, Hingna Road,",review:"2review"},
                    {id:3, img:'assets/images/category/wedding/a10/aa3.jpg', name:"Adarsh Command Service Centre ", phone:"  8888334786.",address:"Snehnagar Mauda Near Bus Stop,",review:"0 review"},
                    {id:4, img:'assets/images/category/wedding/a10/aa4.jpg', name:"Adi N Tech ", phone:"  8552931134.",address:" Mangalmurti square",review:"1 review"},
                    {id:5, img:'assets/images/category/wedding/a10/aa5.jpg', name:"Aryan Internet Cafe", phone:"  7709453001.",address:" Shop No. 5, Rachna Zeel…",review:"1 review"},
                  ] 
              },
              {id:12,img:"assets/images/category/wedding/w12.png",name:"Mandap Decorators",
              sub1:[{id:1, img:'assets/images/category/wedding/a11/aa1.jpg', name :"IT Way Technologies - Web, Software, App Development", phone:"  9970773211..",address:" Mangal Murti Chawk,",review:"1 review"},
                    {id:2, img:'assets/images/category/wedding/a11/aa2.jpg', name:"Aaditya Internet Cafe ", phone:" 9763714087.",address:" Hingna T-Point Square, Hingna Road,",review:"2review"},
                    {id:3, img:'assets/images/category/wedding/a11/aa3.jpg', name:"Adarsh Command Service Centre ", phone:"  8888334786.",address:"Snehnagar Mauda Near Bus Stop,",review:"0 review"},
                    {id:4, img:'assets/images/category/wedding/a11/aa4.jpg', name:"Adi N Tech ", phone:"  8552931134.",address:" Mangalmurti square",review:"1 review"},
                    {id:5, img:'assets/images/category/wedding/a11/aa5.jpg', name:"Aryan Internet Cafe", phone:"  7709453001.",address:" Shop No. 5, Rachna Zeel…",review:"1 review"},
                  ] 
              },
              {id:13,img:"assets/images/category/wedding/w13.png",name:"Marriage Bureaus",
              sub1:[{id:1, img:'assets/images/category/wedding/a12/aa1.jpg', name :"IT Way Technologies - Web, Software, App Development", phone:"  9970773211..",address:" Mangal Murti Chawk,",review:"1 review"},
                    {id:2, img:'assets/images/category/wedding/a12/aa2.jpg', name:"Aaditya Internet Cafe ", phone:" 9763714087.",address:" Hingna T-Point Square, Hingna Road,",review:"2review"},
                    {id:3, img:'assets/images/category/wedding/a12/aa3.jpg', name:"Adarsh Command Service Centre ", phone:"  8888334786.",address:"Snehnagar Mauda Near Bus Stop,",review:"0 review"},
                    {id:4, img:'assets/images/category/wedding/a12/aa4.jpg', name:"Adi N Tech ", phone:"  8552931134.",address:" Mangalmurti square",review:"1 review"},
                    {id:5, img:'assets/images/category/wedding/a12/aa5.jpg', name:"Aryan Internet Cafe", phone:"  7709453001.",address:" Shop No. 5, Rachna Zeel…",review:"1 review"},
                  ] 
              },
              {id:14,img:"assets/images/category/wedding/w14.png",name:"Photo Studios",
              sub1:[{id:1, img:'assets/images/category/wedding/a13/aa1.jpg', name :"IT Way Technologies - Web, Software, App Development", phone:"  9970773211..",address:" Mangal Murti Chawk,",review:"1 review"},
                    {id:2, img:'assets/images/category/wedding/a13/aa2.jpg', name:"Aaditya Internet Cafe ", phone:" 9763714087.",address:" Hingna T-Point Square, Hingna Road,",review:"2review"},
                    {id:3, img:'assets/images/category/wedding/a13/aa3.jpg', name:"Adarsh Command Service Centre ", phone:"  8888334786.",address:"Snehnagar Mauda Near Bus Stop,",review:"0 review"},
                    {id:4, img:'assets/images/category/wedding/a13/aa4.jpg', name:"Adi N Tech ", phone:"  8552931134.",address:" Mangalmurti square",review:"1 review"},
                    {id:5, img:'assets/images/category/wedding/a13/aa5.jpg', name:"Aryan Internet Cafe", phone:"  7709453001.",address:" Shop No. 5, Rachna Zeel…",review:"1 review"},
                  ] 
              },
              {id:15,img:"assets/images/category/wedding/w15.png",name:"Caterers",
              sub1:[{id:1, img:'assets/images/category/wedding/a14/aa1.jpg', name :"IT Way Technologies - Web, Software, App Development", phone:"  9970773211..",address:" Mangal Murti Chawk,",review:"1 review"},
                    {id:2, img:'assets/images/category/wedding/a14/aa2.jpg', name:"Aaditya Internet Cafe ", phone:" 9763714087.",address:" Hingna T-Point Square, Hingna Road,",review:"2review"},
                    {id:3, img:'assets/images/category/wedding/a14/aa3.jpg', name:"Adarsh Command Service Centre ", phone:"  8888334786.",address:"Snehnagar Mauda Near Bus Stop,",review:"0 review"},
                    {id:4, img:'assets/images/category/wedding/a14/aa4.jpg', name:"Adi N Tech ", phone:"  8552931134.",address:" Mangalmurti square",review:"1 review"},
                    {id:5, img:'assets/images/category/wedding/a14/aa5.jpg', name:"Aryan Internet Cafe", phone:"  7709453001.",address:" Shop No. 5, Rachna Zeel…",review:"1 review"},
                  ] 
              },
      
            ]
      },
      {id: 8, img:"assets/images/category/furniture.png", name: "Furniture" ,
      sub : [{id:1,img:"assets/images/category/furniture/f1.png",name:"Homeopathic Doctors"},
              {id:2,img:"assets/images/category/furniture/f2.png",name:"Hearing Aid Center"},
              {id:3,img:"assets/images/category/furniture/f3.png",name:"Acupuncture Doctors"},
              {id:4,img:"assets/images/category/furniture/f4.png",name:"Skin Specialist Doctors"},
              {id:5,img:"assets/images/category/furniture/f5.png",name:"Pediatricians"}, 
           
            ]
      },
      {id: 9, img:"assets/images/category/hospitals.png", name: "Hospitals" ,
      sub : [{id:1,img:"assets/images/category/hospitals/h1.png",name:"Cupping Therapy Clinic"},
              {id:2,img:"assets/images/category/hospitals/h2.png",name:"oxygen cylinder suppliers"},
              {id:3,img:"assets/images/category/hospitals/h3.png",name:"Heart Hospitals"},
              {id:4,img:"assets/images/category/hospitals/h4.png",name:"Acupuncture Hospitals"},
              {id:5,img:"assets/images/category/hospitals/h5.png",name:"Naturopathy Hospitals"}, 
              {id:6,img:"assets/images/category/hospitals/h6.png",name:"Pediatric Hospitals"},
              {id:7,img:"assets/images/category/hospitals/h7.png",name:"Gynaecology Hospitals"},
              {id:8,img:"assets/images/category/hospitals/h8.png",name:"Dental Hospitals"},
              {id:9,img:"assets/images/category/hospitals/h9.png",name:"Neurology Hospitals"},
              {id:10,img:"assets/images/category/hospitals/h10.png",name:"Homeopathy Clinics"},
              {id:11,img:"assets/images/category/hospitals/h11.png",name:"Private Hospitals"},
              {id:12,img:"assets/images/category/hospitals/h12.png",name:"Private Hospitals"},
            ]
      },
      {id: 8, img:"assets/images/category/insurance.png", name: "Insurance" ,
      sub : [{id:1,img:"assets/images/category/insurance/i1.png",name:"Motor Insurance"},
              {id:2,img:"assets/images/category/insurance/i2.png",name:"Life Insurance Agents"},
              {id:3,img:"assets/images/category/insurance/i3.png",name:"Insurance Agents"},
              {id:4,img:"assets/images/category/insurance/i4.png",name:"Health Insurance"},
              {id:5,img:"assets/images/category/insurance/i5.png",name:"LIC Consultant"}, 
              {id:5,img:"assets/images/category/insurance/i6.png",name:"Insurance"}, 
           
            ]
      },
      {id: 8, img:"assets/images/category/restaurants.png", name: "Restaurants" ,
      sub : [ {id:1,img:"assets/images/category/restaurants/r1.png",name:"American Restaurants"},
              {id:2,img:"assets/images/category/restaurants/r2.png",name:"Hyderabadi Restaurants"},
              {id:3,img:"assets/images/category/restaurants/r3.png",name:"Indian Restaurants"},
              {id:4,img:"assets/images/category/restaurants/r4.png",name:"Irani Restaurants"},
              {id:5,img:"assets/images/category/restaurants/r5.png",name:"Italian Restaurants"},
              {id:6,img:"assets/images/category/restaurants/r6.png",name:"Italian Restaurants"}, 
            ]
      },
      {id: 8, img:"assets/images/category/Security-Services.png", name: "Security-Services" ,
      sub : [ {id:1,img:"assets/images/category/security service/s1.png",name:"Events Security"},
              {id:2,img:"assets/images/category/security service/s2.png",name:"Security Guard Training Service"},
              {id:3,img:"assets/images/category/security service/s3.png",name:"Residential Security Services"},
              {id:4,img:"assets/images/category/security service/s4.png",name:"Security Services"},
              {id:5,img:"assets/images/category/security service/s5.png",name:"ATM Security Guard Service"},
              {id:6,img:"assets/images/category/security service/s6.png",name:"Banking Security"},
              {id:7,img:"assets/images/category/security service/s7.png",name:"Commercial Security"},
              {id:8,img:"assets/images/category/security service/s8.png",name:"Security Agencies"}, 
              {id:9,img:"assets/images/category/security service/s9.png",name:"Industrial Security"},
         
            ]
      },
      {id: 9, img:"assets/images/category/tours-travels.png", name: "Tours and Travels" ,
      sub : [ {id:1,img:"assets/images/category/tours and travels/t1.png",name:"Travel Agencies"},
              {id:2,img:"assets/images/category/tours and travels/t2.png",name:"Domestic Tour Operators"},
              {id:3,img:"assets/images/category/tours and travels/t3.png",name:"Tour Operator"},
              {id:4,img:"assets/images/category/tours and travels/t4.png",name:"Travel Agents"},
              {id:5,img:"assets/images/category/tours and travels/t5.png",name:"Tours and Travel Companies"},
              {id:6,img:"assets/images/category/tours and travels/t6.png",name:"Tour Operators"},
              {id:7,img:"assets/images/category/tours and travels/t7.png",name:"Taxi Services Inter City"},
              {id:8,img:"assets/images/category/tours and travels/t8.png",name:"Tours & Travels"}, 
         
            ]
      },
  ];

  getData(){
    return this.cards;
  }

  
  dataSource = new BehaviorSubject(null);
  dataObserver = this.dataSource.asObservable();

  
  dataSourceSubcat = new BehaviorSubject(null);
  dataObserverSubcat = this.dataSourceSubcat.asObservable();

  dataSourceList = new BehaviorSubject(null);
  dataObserverList=this.dataSourceList.asObservable();

  dataSourceAddList =new BehaviorSubject(null);
  dataObserverAddList=this.dataSourceAddList.asObservable();

 
}

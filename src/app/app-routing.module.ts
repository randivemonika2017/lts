import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';
import { ForgotPasswordComponent } from './auth/forgot-password/forgot-password.component';
import { BusinessListGridComponent } from './business-list-grid/business-list-grid.component';
import { BusinessListComponent } from './business-list/business-list.component';
import { BusinessProfileComponent } from './business-profile/business-profile.component';
import { HomeComponent } from './home/home.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { SubCategoryComponent } from './sub-category/sub-category.component';

const routes: Routes = [
    {path: '', redirectTo: '', pathMatch: 'full',component:HomeComponent},
    {path:'category/:subcat', component:SubCategoryComponent},
    {path:'subcat/:subcat',component:BusinessListComponent},
   //{ path: 'admin1', loadChildren: () => import(`./admin/admin.module`).then(m => m.AdminModule) },
    { path: 'login', loadChildren: () => import(`./auth/auth.module`).then(m => m.AuthModule) },
    //{ path: 'salesadmin', loadChildren: () => import(`./sales-admin/sales-admin.module`).then(m => m.SalesAdminModule) },
    {path:'forgotpassword',component:ForgotPasswordComponent},
 // {path:'Businesslist-listview',component:BusinessListComponent},
  //{path:'Businesslist-gridview',component:BusinessListGridComponent},
  {path:':title',component:BusinessProfileComponent},
  {path: '**', component: PageNotFoundComponent},
]
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

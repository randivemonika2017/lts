import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
//import { DataService } from '../customer/data.service';
import { DataService } from '../customer/data.service';

@Component({
  selector: 'app-business-list-grid',
  templateUrl: './business-list-grid.component.html',
  styleUrls: ['./business-list-grid.component.css']
})
export class BusinessListGridComponent implements OnInit {
  businesses: any={};
  url =environment;
  constructor(private router:Router,private dataService:DataService) { }

  ngOnInit(): void {
    
    this.dataService.getAllBusinesses().subscribe(
      data => this.businesses = data
    );
  }

  onSelectBusiness(business: any){
  
    // this.dataService.getAllSubCategories().subscribe(
    //   data => this.subcategories = data
    // );
    // if (business) {
    //   this.dataService.getBusiness(business).subscribe(
    //     data => {
    //       this.businesses = data;
        
    //     }
    //   );
    // } else {
    //   this.businesses = null;
    
    // }
    this.router.navigate([":subcat/:title", business.title]);
  }


  // business(card:any){
   
  //   // this._Service.dataSourceSubcat.next(card);
  //   this.router.navigate([":name/:name",card.name]) ;

  
  // }

}

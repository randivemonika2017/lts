import { Component,OnInit } from '@angular/core';
import {Router} from '@angular/router'
import { HidebannerService } from './cms-pages/hidebanner.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  showSlider=true;
  constructor( public router:Router, private _service:HidebannerService){
  }
  ngOnInit(): void {
    this._service.bannerSource.subscribe((res:boolean)=>{
      this.showSlider=res;
    })
  }

  title = 'local-trade-street';
}

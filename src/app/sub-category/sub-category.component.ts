import { Component, OnInit } from '@angular/core';
import { Router, ParamMap, ActivatedRoute } from '@angular/router';
//import {environment} from 'src/environments/environment';

import {Title} from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { CardService } from 'src/app/card.service';
import { DataService } from '../customer/data.service';

import { environment } from 'src/environments/environment';
//import { DataService } from '../data.service';

@Component({
  selector: 'app-sub-category',
  templateUrl: './sub-category.component.html',
  styleUrls: ['./sub-category.component.css']
})
export class SubCategoryComponent implements OnInit {

//   all_subcategories$: Observable<Category> | undefined 
//   constructor(
//     private route: ActivatedRoute,
//     private router: Router,
//     private cardService: CardService,
//     private titleService: Title
//     ) { }

//   ngOnInit() {
//     // this.all_subcategories$ = this.route.paramMap.pipe(
//     //   switchMap((params: ParamMap) =>
//     //     this.cardService.getAllSubCategories(+params.get('id'))
//     //   )
//     // );

//     this.titleService.setTitle('Sub Category');
//   }

// }



para: any;
url =environment;
  data:any;
  subcategories: any={};
  categories: any={};;
  constructor(private _service:CardService ,private router: Router,private dataService:DataService,private route:ActivatedRoute) { 

    this.route.params.subscribe((dt)=>{
      this.para = dt;
      console.log(this.para);
    })
    
  }

  ngOnInit(): void {
   // this.data = this._service.cards;
    
    this._service.dataSource.subscribe((res:any)=>{
      this.data=res;
    });
    this.dataService.getSubCategories(this.para.subcat).subscribe(
      data => this.subcategories = data
    );
    // this.dataService.getcategories().subscribe(
    //   data => this.categories = data
    // );

  }
  onSelectSubcategory(subcategory: any){
  
    // this.dataService.getAllSubCategories().subscribe(
    //   data => this.subcategories = data
    // );
    if (subcategory) {
      this.dataService.getBusinessList(subcategory).subscribe(
        data => {
          this.subcategories = data;
        
        }
      );
    } else {
      this.subcategories = null;
    
    }
    this.router.navigate(["/subcat", subcategory.subcat]);
  }

  // business(card:any){
   
  //   this._service.dataSourceSubcat.next(card);
  //   this.router.navigate(["/:name",card.name]) ;

  
  // }

}
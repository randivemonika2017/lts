import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAddlistingComponent } from './admin-addlisting.component';

describe('AdminAddlistingComponent', () => {
  let component: AdminAddlistingComponent;
  let fixture: ComponentFixture<AdminAddlistingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AdminAddlistingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAddlistingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

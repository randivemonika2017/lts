import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataService } from 'src/app/customer/data.service';

@Component({
  selector: 'app-admin-addlisting',
  templateUrl: './admin-addlisting.component.html',
  styleUrls: ['./admin-addlisting.component.css']
})
export class AdminAddlistingComponent implements OnInit {

selectedState = 0;
  days=[{id:1,dayName:"Monday"},
              {id:2,dayName:"Tuesday"},
              {id:3,dayName:"Wednesday"},
              {id:4,dayName:"Thursday"},
              {id:5,dayName:"Satarday"},
              {id:6,dayName:"Sunday"},
            ];
  
openingtimes=[{id:1,time:"1 AM"},
                {id:2,time:"2 AM"},
                {id:3,time:"3 AM"},
                {id:4,time:"4 AM"},
                {id:5,time:"5 AM"},
                {id:6,time:"6 AM"},
                {id:7,time:"7 AM"},
                {id:8,time:"8 AM"},
                {id:9,time:"9 AM"},
                {id:10,time:"10 AM"},
                {id:11,time:"11 AM"},
                {id:12,time:"12 AM"},
                {id:13,time:"1 PM"},
                {id:14,time:"2 PM"},
                {id:15,time:"3 PM"},
                {id:16,time:"4 PM"},
                {id:17,time:"5 PM"},
                {id:18,time:"6 PM"},
                {id:19,time:"7 PM"},
                {id:20,time:"8 PM"},
                {id:21,time:"9 PM"},
                {id:22,time:"10 PM"},
                {id:23,time:"11 PM"},
                {id:24,time:"12 PM"}
              ];
  
closingtimes=[{id:1,time:"1 AM"},
                {id:2,time:"2 AM"},
                {id:3,time:"3 AM"},
                {id:4,time:"4 AM"},
                {id:5,time:"5 AM"},
                {id:6,time:"6 AM"},
                {id:7,time:"7 AM"},
                {id:8,time:"8 AM"},
                {id:9,time:"9 AM"},
                {id:10,time:"10 AM"},
                {id:11,time:"11 AM"},
                {id:12,time:"12 AM"},
                {id:13,time:"1 PM"},
                {id:14,time:"2 PM"},
                {id:15,time:"3 PM"},
                {id:16,time:"4 PM"},
                {id:17,time:"5 PM"},
                {id:18,time:"6 PM"},
                {id:19,time:"7 PM"},
                {id:20,time:"8 PM"},
                {id:21,time:"9 PM"},
                {id:22,time:"10 PM"},
                {id:23,time:"11 PM"},
                {id:24,time:"12 PM"}
              ];

  categories:any= {};
  subcategories :any={};
  // days:any={};
  // openingtimes:any={};
  // closingtimes:any={};
  addListingForm:any=FormGroup;
  subcategorySettings:any={}
  disabled = false;
  ShowFilter = false;
  limitSelection = false;
  selectedFile:any=null;
  category:any=[];
  Subcategory: any = [];
  selectedItems: any = [];
 
  submitted = false;
  error: {} | undefined;
 
states :any = [];
cities:any = [];
  selectedCategory: any;
 
  alert : boolean=false
  images: any=[];

  constructor(private dataService: DataService,public fb:FormBuilder,private _http:HttpClient) { 
      this.addListingForm = this.fb.group({
      sales_id: ['', Validators.required],
      title: ['', Validators.required],
      tag_line: ['',Validators.required],
      logo:['',Validators.required],
      banner:['',Validators.required],
      category:['',Validators.required],
      subcat:['',Validators.required],
      address: ['',Validators.required],
      latitude: ['',Validators.required],
      longitude: ['',Validators.required],
      state:['',Validators.required],
      city:['',Validators.required],
      zip_code: ['',Validators.required],
      p_mobile: ['',Validators.required],
      b_mobile: ['',Validators.required],
      b_mobile2: ['',Validators.required],
      email:['',Validators.required],
      website:['',Validators.required],
      facebook:['',Validators.required],
      linkdin:['',Validators.required],
      twitter:['',Validators.required],
      instagram:['',Validators.required],
      descri: [''],
      mon_o:[''],
      mon_c:[''],
      tue_o:[''],
      tue_c:[''],
      wed_o:[''],
      wed_c:[''],
      thrus_o:[''],
      thrus_c:[''],
      fri_o:[''],
      fri_c:[''],
      sat_o:[''],
      sat_c:[''],
      sun_o:[''],
      sun_c:[''],
      video_url: [''],
      img_gallary: [''],
      fileSource: ['', Validators.required],
      status:[''],
     })
  
  
  }


  ngOnInit() {
    this.dataService.getcategories().subscribe(
    data => this.categories = data
  );

  }
  onChangeCategory(event:any) {
    let cat_id=event.target.value;
     if (cat_id) {
       this.dataService.getSubcategories(cat_id).subscribe(
         data => {
           this.subcategories = data;
          
         }
       );
     } else {
       this.subcategories = null;
     }
   }




    getstates() {
    return [
    { id: 1, name: 'Maharashtra' },
    { id: 2,  name: 'Madhya Pradesh' },]
  }
  
   getCity() {
    return [{id:1, stateId: 1,name:"Nagpur"},
            {id:2, stateId: 1,name:"Pune"},
            {id:3, stateId: 1,name:"Mumbai"},
            {id:4, stateId: 1,name:"Nashik"},
            {id:4, stateId: 2,name:"Bhopal"},
            {id:4, stateId: 2,name:"Satna"},
            {id:4, stateId: 2,name:"Indor"},
          ];

  }
  
  onSelectState(event: any){
    let stateId=event.target.value;
    this.selectedState = stateId;
    this.cities = this.getCity().filter((item) => {
    return item.stateId === Number(stateId)
    });
  }
  
  
onFileUpload1(event: any){
  if(event.target.files.length>0){
    const file1=event.target.files[0];
    this.addListingForm.get('logo').setValue(file1);
   
  }
}

onFileUpload2(event: any){
  if(event.target.files.length>0){
    const file2=event.target.files[0];
    this.addListingForm.get('banner').setValue(file2);
   
  }
}
  onFileSelected(event: any){
    if(event.target.files.length>0){
      const file3=event.target.files[0];
      this.addListingForm.get('img_gallary').setValue(file3);
    }
    
  }
 
 
  onSubmit(){
    const fd=new FormData();
    fd.append('sales_id',this.addListingForm.get('sales_id').value);
    fd.append('title',this.addListingForm.get('title').value);
    fd.append('tag_line',this.addListingForm.get('tag_line').value);
    fd.append('logo',this.addListingForm.get('logo').value);
    fd.append('banner',this.addListingForm.get('banner').value);
    fd.append('category',this.addListingForm.get('category').value);
    fd.append('subcat',this.addListingForm.get('subcat').value);
    fd.append('address',this.addListingForm.get('address').value);
    fd.append('latitude',this.addListingForm.get('latitude').value);
    fd.append('longitude',this.addListingForm.get('longitude').value);    
    fd.append('state',this.addListingForm.get('state').value);
    fd.append('city',this.addListingForm.get('city').value);
    fd.append('zip_code',this.addListingForm.get('zip_code').value);
    fd.append('p_mobile',this.addListingForm.get('p_mobile').value);
    fd.append('b_mobile',this.addListingForm.get('b_mobile').value);
    fd.append('b_mobile2',this.addListingForm.get('b_mobile2').value);
    fd.append('email',this.addListingForm.get('email').value);
    fd.append('website',this.addListingForm.get('website').value);
    fd.append('facebook',this.addListingForm.get('facebook').value);
    fd.append('linkdin',this.addListingForm.get('linkdin').value);
    fd.append('twitter',this.addListingForm.get('twitter').value);
    fd.append('instagram',this.addListingForm.get('instagram').value);
    fd.append('descri',this.addListingForm.get('descri').value);
    fd.append('mon_o',this.addListingForm.get('mon_o').value);
    fd.append('mon_c',this.addListingForm.get('mon_c').value);
    fd.append('tue_o',this.addListingForm.get('tue_o').value);
    fd.append('tue_c',this.addListingForm.get('tue_c').value);
    fd.append('wed_o',this.addListingForm.get('wed_o').value);
    fd.append('wed_c',this.addListingForm.get('wed_c').value);
    fd.append('thrus_o',this.addListingForm.get('thrus_o').value);
    fd.append('thrus_c',this.addListingForm.get('thrus_c').value);
    fd.append('fri_o',this.addListingForm.get('fri_o').value);
    fd.append('fri_c',this.addListingForm.get('fri_c').value);
    fd.append('sat_o',this.addListingForm.get('sat_o').value);
    fd.append('sat_c',this.addListingForm.get('sat_c').value);
    fd.append('sun_o',this.addListingForm.get('sun_o').value);
    fd.append('sun_c',this.addListingForm.get('sun_c').value);
    fd.append('video_url',this.addListingForm.get('video_url').value);
    fd.append('img_gallary',this.addListingForm.get('img_gallary').value);
    fd.append('status',this.addListingForm.get('status').value);
    // selectedFile,this.selectedFile.name);
    console.log(this.addListingForm.value);
    this.dataService.submitListing(fd).subscribe(
    (res) => console.log(res),
    (err)=>console.log(err)
    )
    alert('Submitt Successfully.');
    
  }
  closeAlert(){
    this.alert=false
    
  }

}

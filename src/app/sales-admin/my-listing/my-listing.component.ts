import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/customer/data.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-my-listing',
  templateUrl: './my-listing.component.html',
  styleUrls: ['./my-listing.component.css']
})
export class MyListingComponent implements OnInit {

  businesses:  any={};
  url =environment;
  constructor(private dataService:DataService ) { }

  ngOnInit(): void {
    this.dataService.getAllBusinesses().subscribe(
      data => this.businesses = data
    );

  }

}

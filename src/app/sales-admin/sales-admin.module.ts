import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SalesAdminRoutingModule } from './sales-admin-routing.module';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component'
import { AdminComponent } from './admin/admin.component';
import { MyListingComponent } from './my-listing/my-listing.component';
import { AdminProfileComponent } from './admin-profile/admin-profile.component';
import { AdminAddlistingComponent } from './admin-addlisting/admin-addlisting.component';


@NgModule({
  declarations: [
    AdminDashboardComponent,AdminAddlistingComponent, AdminComponent,   MyListingComponent, AdminProfileComponent ],
  imports: [
    CommonModule,
    SalesAdminRoutingModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers:[FormBuilder]
})
export class SalesAdminModule { }

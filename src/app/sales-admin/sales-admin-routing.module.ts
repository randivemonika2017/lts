import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../auth/auth.guard';
import { AdminAddlistingComponent } from './admin-addlisting/admin-addlisting.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { AdminProfileComponent } from './admin-profile/admin-profile.component';
import { AdminComponent } from './admin/admin.component';
import { MyListingComponent } from './my-listing/my-listing.component';

const routes: Routes = [{
  path: 'salesadmin',
  component: AdminComponent,
  canActivate: [AuthGuard],
  children: [
    {
    path: '',
    children: [
      { path: 'addlisting', component: AdminAddlistingComponent },
      { path: 'mylisting', component : MyListingComponent},
      { path: 'profile', component:AdminProfileComponent},
      
      //{path:'saleslogin',component:SignInComponent},
     
     // {path:'saleslogin',component:SignInComponent},
      { path: '', component: AdminDashboardComponent },
    
    ],
  }
],
}
]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SalesAdminRoutingModule { }

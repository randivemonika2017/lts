import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CardService } from '../card.service';
import { HidebannerService } from '../cms-pages/hidebanner.service';
//import { DataService } from '../customer/data.service';
import {environment} from 'src/environments/environment';

import { HttpClient, HttpParams } from '@angular/common/http';
import { DataService } from '../customer/data.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {


  
  data: any = [];
  categories: any=[];
  selectedCategory: any;
  subcategories: any=[];
  url =environment;

  // navigate(card: any) {

  //   this.service.dataSource.next(card);
  //   this.router.navigate(["/category", card.name]);
  // }


  showSlider=true;
  constructor( public router:Router,private http: HttpClient, private _service:HidebannerService,private service: CardService,private dataService:DataService){
  }
  ngOnInit() {

    this._service.bannerSource.subscribe((res:boolean)=>{
      this.showSlider=res;
    })
    this.getAllSubcategories();
    
    // this.dataService.getSubCategories(category).subscribe(
    //   data => {
    //     this.subcategories = data;
      
    //   }
    // );
    
  }

getAllCategories(){
  this.dataService.getcategories().subscribe(
    data => this.categories = data
  );
}

  getAllSubcategories(){
    this.dataService.getAllSubCategories().subscribe(
      data => {this.subcategories = data;
      this.getAllCategories();
      }
    );
  }
  filterByCategory(categoryId:any){
    return this.subcategories.filter((cat: { cat_id: any; }) => cat.cat_id == categoryId)
}
  onSelectCategory(category: any):void{
      this.router.navigate(["/category/", category.category]);
    }
    subcatroute(subcat :any){
      this.router.navigate(["/subcat/", subcat]);
    }

}

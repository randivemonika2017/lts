import { BrowserModule,Title } from '@angular/platform-browser';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA, NO_ERRORS_SCHEMA  } from '@angular/core';
import {CustomerModule} from './customer/customer.module';
import { AdminModule } from './admin/admin.module';
import {SalesAdminModule} from './sales-admin/sales-admin.module';
//import {SalesAdminModule} from './sales-admin/sales-admin.module';
import {CmsPagesModule} from './cms-pages/cms-pages.module'
import {HttpClientModule} from '@angular/common/http';
import {FormArray, FormBuilder, ReactiveFormsModule,FormsModule} from '@angular/forms';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { YouTubePlayerModule } from "@angular/youtube-player";
import {GoogleMapsModule} from '@angular/google-maps';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { BannerComponent } from './banner/banner.component';
import { CategorySliderComponent } from './category-slider/category-slider.component';
import { FooterComponent } from './footer/footer.component';
//import { CategoryComponent } from './category/category.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { HomeComponent } from './home/home.component';
import { httpInterceptorProviders} from './http-interceptors/index';
  import { from } from 'rxjs';
  import { FileUploadModule } from 'ng2-file-upload';
import { SubCategoryComponent } from './sub-category/sub-category.component';
import { BusinessListComponent } from './business-list/business-list.component';
import { BusinessListGridComponent } from './business-list-grid/business-list-grid.component';
import { BusinessProfileComponent } from './business-profile/business-profile.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    BannerComponent,
    CategorySliderComponent,
    SubCategoryComponent,
    FooterComponent,
    PageNotFoundComponent,
    HomeComponent,
    BusinessListComponent,
    BusinessListGridComponent,
    BusinessProfileComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    FileUploadModule,
    HttpClientModule,
    ReactiveFormsModule,
    CustomerModule,
    AdminModule,
    SalesAdminModule,
    CmsPagesModule,
    NgMultiSelectDropDownModule,
    YouTubePlayerModule,
    GoogleMapsModule,
    AppRoutingModule,

  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA,NO_ERRORS_SCHEMA],
  providers: [Title,FormBuilder,httpInterceptorProviders,{provide:LocationStrategy,useClass:HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { }

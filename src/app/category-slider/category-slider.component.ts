import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { DataService } from '../customer/data.service';

@Component({
  selector: 'app-category-slider',
  templateUrl: './category-slider.component.html',
  styleUrls: ['./category-slider.component.css']
})
export class CategorySliderComponent implements OnInit {
  categories: any=[];
  url =environment;
  constructor(private dataService:DataService) { }

  ngOnInit(): void {
    this.dataService.getcategories().subscribe(
      data => this.categories = data
    );
  }

}

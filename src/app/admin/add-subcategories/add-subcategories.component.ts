import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DataService } from 'src/app/customer/data.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-add-subcategories',
  templateUrl: './add-subcategories.component.html',
  styleUrls: ['./add-subcategories.component.css']
})
export class AddSubcategoriesComponent implements OnInit {

  addSubcategoryForm: any=FormGroup;
  categories: any={};
  selectedCategory: any;
  url =environment;
  alert : boolean=false
  subcategories: any;
  constructor(private dataService: DataService,public fb:FormBuilder,private _http:HttpClient) { 
    this.addSubcategoryForm = this.fb.group({
      cat_id: ['', Validators.required],
      subcat: ['', Validators.required],
      subcat_img:['',Validators.required],
    })
  }
  ngOnInit(): void {
    this.dataService.getcategories().subscribe(
      data => this.categories = data
    );
    this.dataService.getAllSubCategories().subscribe(
      data => this.subcategories = data
    );
    

  }


  onSelectCategory(event: any){
    let categoryId=event.target.value;
    this.selectedCategory = categoryId;
   
    this.subcategories= this.addSubcategoryForm.get('cat_id').value;
  }
  deleteSubcategory(subcategory:any){
    this.dataService.deleteSubcategory(subcategory.id)
        .subscribe(response => {
          this.subcategories = this.subcategories.filter((item:any) => item.id !== subcategory.id);
        });
  }

  onFileSelected(event: any){
    if(event.target.files.length>0){
      const file=event.target.files[0];
      this.addSubcategoryForm.get('subcat_img').setValue(file);
    }
    // this.selectedFile=<File>event.target.files[0];
  }
  onSubmit(){
    const fd=new FormData();
    fd.append('cat_id',this.addSubcategoryForm.get('cat_id').value);
    fd.append('subcat',this.addSubcategoryForm.get('subcat').value);
    fd.append('subcat_img',this.addSubcategoryForm.get('subcat_img').value);
    this._http.post('http://localtradestreet.com/ltsapi/public/api/subcat',fd).subscribe(
      (res) => console.log(res),
      (err)=>console.log(err)
      )
      this.dataService.getAllSubCategories().subscribe(
        data => this.subcategories = data
      );
      
  }
  closeAlert(){
    this.alert=false
  }

}

import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from 'src/app/customer/data.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-add-categories',
  templateUrl: './add-categories.component.html',
  styleUrls: ['./add-categories.component.css']
})
export class AddCategoriesComponent implements OnInit {

  url =environment;
  addCategoryForm:any=FormGroup;
  categories: any={};
  alert : boolean=false
 
  constructor(private dataService: DataService,public fb:FormBuilder,private _http:HttpClient,private router:Router) { 
    this.addCategoryForm = this.fb.group({
      category: ['', Validators.required],
      cat_img:['',Validators.required],
    })
  }
  ngOnInit(): void {
    this.dataService.getcategories().subscribe(
      data => this.categories = data
    );

  }
  onFileSelected(event: any){
    if(event.target.files.length>0){
      const file=event.target.files[0];
      this.addCategoryForm.get('cat_img').setValue(file);
    }
    // this.selectedFile=<File>event.target.files[0];
  }
  deleteCategory(category:any){
      this.dataService.deleteCategory(category.id)
          .subscribe(response => {
            this.categories = this.categories.filter((item:any) => item.id !== category.id);
          });
    }
  // onedit(category:any){
  //   this.router.navigate([":id", category.id]);
  // }

  onSubmit(){
    const fd=new FormData();
    fd.append('category',this.addCategoryForm.get('category').value);
    fd.append('cat_img',this.addCategoryForm.get('cat_img').value);
    this._http.post('http://127.0.0.1:8000/api/cat',fd).subscribe(
    // this._http.post('http://localtradestreet.com/ltsapi/public/api/cat',fd).subscribe(
      (res) => {this.alert=true,
        console.log(res)},
      (err)=>console.log(err)
    )
    this.dataService.getcategories().subscribe(
      data => this.categories = data
    );
  }
  closeAlert(){
    this.alert=false
  }
}

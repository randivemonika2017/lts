import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/customer/data.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-update-subcategory',
  templateUrl: './update-subcategory.component.html',
  styleUrls: ['./update-subcategory.component.css']
})
export class UpdateSubcategoryComponent implements OnInit {

  url =environment;
  updateSubcategoryForm:any=FormGroup;
 
  alert : boolean=false

  constructor(private arouter:ActivatedRoute,private dataService: DataService,public fb:FormBuilder,private _http:HttpClient,private router:Router) { 
    debugger
    this.updateSubcategoryForm = this.fb.group({
      subcat: ['', Validators.required],
      subcat_img:['',Validators.required],
    })
  }
  ngOnInit(): void {
    console.warn(this.arouter.snapshot.params.id)
    this.dataService.getCurrentSubcategory(this.arouter.snapshot.params.id).subscribe((result)=>{
      console.warn(result )
      this.updateSubcategoryForm = this.fb.group({
        subcat: ['', Validators.required],
        subcat_img:['',Validators.required],
      })

        this.updateSubcategoryForm.patchValue(result)
       
      })
  }
  onFileSelected(event: any){
    if(event.target.files.length>0){
      const file=event.target.files[0];
      this.updateSubcategoryForm.get('subcat_img').setValue(file);
    }
    // this.selectedFile=<File>event.target.files[0];
  }
  onSubmit(){
   
  console.warn("item",this.updateSubcategoryForm.value)
  const fd=new FormData();
    fd.append('subcat',this.updateSubcategoryForm.get('subcat').value);
    fd.append('subcat_img',this.updateSubcategoryForm.get('subcat_img').value);

        this.dataService.UpdateSubcategory(this.arouter.snapshot.params.id,fd).subscribe(
       
          (res) => {this.alert=true,
            console.log(res)},
          (err)=>console.log(err)
        )
        console.log(this.arouter.snapshot.params.id)
        this.router.navigate(['admin/addsubcategory']);
      }
      closeAlert(){
        this.alert=false
      }
}

import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
//import { environment } from 'src/environments/environment';
import { User } from '../models';

@Injectable({
  providedIn: 'root'
})
export class UserService {
    public apiUrl= 'http://127.0.0.1:8000/api/';

   // apiUrl: 'http://127.0.0.1:8000/api/'
  constructor(private http: HttpClient) { }

//   getAll() {
//       return this.http.get<User[]>(`${this.apiUrl}/users`);
//   }

//   getById(id: number) {
//       return this.http.get(`${this.apiUrl}/users/${id}`);
//   }

  salesregister(user: User) {
      return this.http.post(`${this.apiUrl}sales`, user);
  }
  register(user: User) {
    return this.http.post(`${this.apiUrl}register`, user);
}
 
  //http://127.0.0.1:8000/api/register", fd
//   update(user: User) {
//       return this.http.put(`${this.apiUrl}/users/${user.id}`, user);
//   }

//   delete(id: number) {
//       return this.http.delete(`${this.apiUrl}/users/${id}`);
//   }
}

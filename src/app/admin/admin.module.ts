import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormBuilder, FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminRoutingModule } from './admin-routing.module';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
import { AdminComponent } from './admin/admin.component';
import { AddCategoriesComponent } from './add-categories/add-categories.component';
import { AddSubcategoriesComponent } from './add-subcategories/add-subcategories.component';
import { MyListingComponent } from './my-listing/my-listing.component';
import { AdminProfileComponent } from './admin-profile/admin-profile.component';
import { AddSellsPersonComponent } from './add-sells-person/add-sells-person.component';
import { UpdateCategoryComponent } from './update-category/update-category.component';
import { UpdateSubcategoryComponent } from './update-subcategory/update-subcategory.component';



@NgModule({
  declarations: [AdminDashboardComponent, AdminComponent, AddCategoriesComponent, AddSubcategoriesComponent,  MyListingComponent, AdminProfileComponent, AddSellsPersonComponent, UpdateCategoryComponent, UpdateSubcategoryComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,
    FormsModule,
     ReactiveFormsModule
  ],
  providers:[FormBuilder]
})
export class AdminModule { }

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddSellsPersonComponent } from './add-sells-person.component';

describe('AddSellsPersonComponent', () => {
  let component: AddSellsPersonComponent;
  let fixture: ComponentFixture<AddSellsPersonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddSellsPersonComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddSellsPersonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { first } from 'rxjs/operators';
import { AuthService } from 'src/app/auth/auth.service';
import { DataService } from 'src/app/customer/data.service';
import { AlertService } from '../services/alert.service';
//import { AuthenticationService } from '../services/authentication.service';
import { UserService } from '../services/user.service';
//import { DataService } from 'src/app/customer/data.service';
//import { AlertService } from '../services/alert.service';
//import { AuthenticationService } from '../services/authentication.service';
//import { UserService } from '../services/user.service';

@Component({
  selector: 'app-add-sells-person',
  templateUrl: './add-sells-person.component.html',
  styleUrls: ['./add-sells-person.component.css']
})
export class AddSellsPersonComponent implements OnInit {

    
  
  addSalesEmployeeForm:any=FormGroup;
  
  disabled = false;
  ShowFilter = false;
  limitSelection = false;
  selectedFile:any=null;

 
  Role: any = ['Sale-Admin', 'Admin'];

  submitted = false;
  error: {} | undefined;
 

 
 
  alert : boolean=false
  loading: boolean=false;
  
  // constructor(public fb:FormBuilder,private _http:HttpClient,
  //   private router: Router,
  //   private dataService: DataService) {   }

  constructor(
    private fb: FormBuilder,
    private router: Router,
    //private authenticationService: AuthenticationService,
   // private userService: UserService,
    private alertService: AlertService,
    private _http:HttpClient,
    private authservice:AuthService
) { 
    // redirect to home if already logged in
    // if (this.authenticationService.currentUserValue) { 
    //     this.router.navigate(['/']);
    // }
}

  onFileUpload1(event: any){
    if(event.target.files.length>0){
      const file1=event.target.files[0];
      this.addSalesEmployeeForm.get('avatar').setValue(file1);
     
    }
}
ngOnInit() {
  this.addSalesEmployeeForm = this.fb.group({
    role:['',Validators.required],
      fname: ['', Validators.required],
      lname: ['',Validators.required],
      avatar:['',Validators.required],
      address:['',Validators.required],
      state:['',Validators.required],
      city:['',Validators.required],
      mobile: ['',Validators.required],
      email:['',Validators.required],
      password:['',[Validators.required, Validators.minLength(6)]],
      c_password:['',[Validators.required, Validators.minLength(6)]],  });
 

  }
  changeRole(e:any) {
    console.log(e.value)
    this.Role.setValue(e.target.value, {
      onlySelf: true
    })
  }
  get role() {
    return this.addSalesEmployeeForm.get('role');
  }
  
  get f() { return this.addSalesEmployeeForm.controls; }

  onSubmit(){
    this.submitted=true;
//     this.submitted = true;

//     //stop here if form is invalid
//     if (this.addSalesEmployeeForm.invalid) {
//         return;
//     }

//    this.loading = true;
//     const fd=new FormData();
//     fd.append('sp_fname',this.addSalesEmployeeForm.get('sp_fname').value);
//     fd.append('sp_lname',this.addSalesEmployeeForm.get('sp_lname').value);
//     fd.append('sp_avatar',this.addSalesEmployeeForm.get('sp_avatar').value);
//     fd.append('sp_address',this.addSalesEmployeeForm.get('sp_address').value);
//     fd.append('sp_state',this.addSalesEmployeeForm.get('sp_state').value);
//     fd.append('sp_city',this.addSalesEmployeeForm.get('sp_city').value);
//     fd.append('sp_mobile',this.addSalesEmployeeForm.get('sp_mobile').value);
//     fd.append('sp_email',this.addSalesEmployeeForm.get('sp_email').value);
//     fd.append('sp_password',this.addSalesEmployeeForm.get('sp_password').value);
    
//     this._http.post("http://127.0.0.1:8000/api/sales",fd)
// //this.userService.register(this.addSalesEmployeeForm.value)
//         .pipe(first())
//         .subscribe(
//             data => {
//                 this.alertService.success('Registration successful', true);
//                 this.router.navigate(['/login']);
//             },
//             error => {
//                 this.alertService.error(error);
//                 this.loading = false;
//             });
// }



  //   debugger
  //  console.log(this.addSalesEmployeeForm.value);
    const fd=new FormData();
    fd.append('role',this.addSalesEmployeeForm.get('role').value);
    fd.append('fname',this.addSalesEmployeeForm.get('fname').value);
    fd.append('lname',this.addSalesEmployeeForm.get('lname').value);
    fd.append('avatar',this.addSalesEmployeeForm.get('avatar').value);
    fd.append('address',this.addSalesEmployeeForm.get('address').value);
    fd.append('state',this.addSalesEmployeeForm.get('state').value);
    fd.append('city',this.addSalesEmployeeForm.get('city').value);
    fd.append('mobile',this.addSalesEmployeeForm.get('mobile').value);
    fd.append('email',this.addSalesEmployeeForm.get('email').value);
    fd.append('password',this.addSalesEmployeeForm.get('password').value);
    fd.append('c_password',this.addSalesEmployeeForm.get('c_password').value);
    
    //  console.log(this.addSalesEmployeeForm.value);
     this._http.post("http://localtradestreet.com/ltsapi/public/api/register", fd).subscribe(
    // this.dataService.register(fd).subscribe(
    (res) =>{ console.log(res);
               this.router.navigate(['login']);},
    (err)=>console.log(err)
    )
    alert('Submit Successfully.');
    
  }
  closeAlert(){
    this.alert=false
    
  }

}

 
  

import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/customer/data.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-my-listing',
  templateUrl: './my-listing.component.html',
  styleUrls: ['./my-listing.component.css']
})
export class MyListingComponent implements OnInit {

  businesses:  any={};
  url =environment;
  constructor(private dataService:DataService ) { }
  deleteBusiness(business:any){
    this.dataService.deleteBusiness(business.id)
        .subscribe(response => {
          this.businesses = this.businesses.filter((item:any) => item.id !== business.id);
        });
  }
  changed(business: any) {
    if (business.status === 1) {
      business.status = 0;
    } else if (business.status === 0) {
      business.status = 1;
    
  }
  }
  ngOnInit(): void {
    this.dataService.getAllBusinesses().subscribe(
      data => this.businesses = data
    );

  }

}

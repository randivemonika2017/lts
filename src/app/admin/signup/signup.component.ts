import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  SignUpForm:any= FormGroup;
   submitted = false;
   returnUrl: any;
   error:any= {};
   loginError: any;
   message=false;
   Role: any = ['Sale-Admin', 'Admin'];

  alert : boolean=false
 
   constructor(
     private fb: FormBuilder,
     private router: Router,
     private authService: AuthService,
     private _http:HttpClient
     ) { }
 
   ngOnInit() {
    this.SignUpForm = this.fb.group({
      role:['',Validators.required],
      fname: ['', Validators.required],
      lname: ['',Validators.required],
      avatar:['',Validators.required],
      address:['',Validators.required],
      state:['',Validators.required],
      city:['',Validators.required],
      mobile: ['',Validators.required],
      email:['',Validators.required],
      password:['',[Validators.required, Validators.minLength(6)]],
      c_password:['',[Validators.required, Validators.minLength(6)]],     });

   }
   changeRole(e:any) {
    console.log(e.value)
    this.Role.setValue(e.target.value, {
      onlySelf: true
    })
  }
  onFileUpload1(event: any){
    if(event.target.files.length>0){
      const file1=event.target.files[0];
      this.SignUpForm.get('avatar').setValue(file1);
     
    }
}
  get role() {
    return this.SignUpForm.get('role');
  }
   get f() { return this.SignUpForm.controls; }
 onSubmit() {
   this.submitted=true;
  const fd=new FormData();
  fd.append('role',this.SignUpForm.get('role').value);
  fd.append('fname',this.SignUpForm.get('fname').value);
  fd.append('lname',this.SignUpForm.get('lname').value);
  fd.append('avatar',this.SignUpForm.get('avatar').value);
  fd.append('address',this.SignUpForm.get('address').value);
  fd.append('state',this.SignUpForm.get('state').value);
  fd.append('city',this.SignUpForm.get('city').value);
  fd.append('mobile',this.SignUpForm.get('mobile').value);
  fd.append('email',this.SignUpForm.get('email').value);
  fd.append('password',this.SignUpForm.get('password').value);
  fd.append('c_password',this.SignUpForm.get('c_password').value);
  
  //  console.log(this.addSalesEmployeeForm.value);
   this._http.post("http://localtradestreet.com/ltsapi/public/api/register", fd).subscribe(
  // this.dataService.register(fd).subscribe(
  (res) =>{ console.log(res);
             this.router.navigate(['login']);},
  (err)=>console.log(err)
  )
  alert('Submit Successfully.');
  
}
closeAlert(){
  this.alert=false
  
}

}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { AdminDashboardComponent } from './admin-dashboard/admin-dashboard.component';
//import {SalesRegisterComponent} from './sales-register/sales-register.component';
import {AddSellsPersonComponent} from './add-sells-person/add-sells-person.component';
import {AddCategoriesComponent} from './add-categories/add-categories.component';
import {AddSubcategoriesComponent} from './add-subcategories/add-subcategories.component';
import { MyListingComponent } from './my-listing/my-listing.component';
import { AuthGuard } from '../auth/auth.guard';
import { from } from 'rxjs';
import { AdminProfileComponent } from './admin-profile/admin-profile.component';
import { SignupComponent } from './signup/signup.component';
import { UpdateCategoryComponent } from './update-category/update-category.component';
import { UpdateSubcategoryComponent } from './update-subcategory/update-subcategory.component';
;


const routes: Routes = [
  {
    // path: '',
    // component: AdminComponent,
    // canActivate:[AuthGuard],
    // canActivate: [AuthGuard],
    // children: [
    //   {
      path: 'admin',component:AdminComponent,
      children: [
        { path: 'addcategory', component: AddCategoriesComponent },
        {path:'catupdate/:id',component:UpdateCategoryComponent},
        {path:'subcatupdate/:id',component:UpdateSubcategoryComponent},
        { path: 'addsubcategory', component: AddSubcategoriesComponent },
        { path: 'salesregister', component: AddSellsPersonComponent },
        { path: 'mylisting', component : MyListingComponent},
        { path: 'profile', component:AdminProfileComponent},
        {path:'signup', component:SignupComponent},
        //{path:'saleslogin',component:SignInComponent},
       
       // {path:'saleslogin',component:SignInComponent},
        { path: '', component: AdminDashboardComponent },
      
      ],
    }
  ]
//   }
// ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }

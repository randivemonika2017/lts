import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/customer/data.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-update-category',
  templateUrl: './update-category.component.html',
  styleUrls: ['./update-category.component.css']
})
export class UpdateCategoryComponent implements OnInit {
  url =environment;
  updateCategoryForm:any=FormGroup;
  categories: any={};
  alert : boolean=false

  constructor(private arouter:ActivatedRoute,private dataService: DataService,public fb:FormBuilder,private _http:HttpClient,private router:Router) { 
    debugger
    this.updateCategoryForm = this.fb.group({
      category: ['', Validators.required],
      cat_img:['',Validators.required],
    })
  }
  ngOnInit(): void {
    console.warn(this.arouter.snapshot.params.id)
    this.dataService.getCurrentCategory(this.arouter.snapshot.params.id).subscribe((result)=>{
      console.warn(result )
      this.updateCategoryForm = this.fb.group({
        category: ['', Validators.required],
        cat_img:['',Validators.required],
      })

        this.updateCategoryForm.patchValue(result)
       
      })
  }
  onFileSelected(event: any){
    if(event.target.files.length>0){
      const file=event.target.files[0];
      this.updateCategoryForm.get('cat_img').setValue(file);
    }
    // this.selectedFile=<File>event.target.files[0];
  }
  onSubmit(){
   
  console.warn("item",this.updateCategoryForm.value)
  const fd=new FormData();
    fd.append('category',this.updateCategoryForm.get('category').value);
    fd.append('cat_img',this.updateCategoryForm.get('cat_img').value);

        this.dataService.UpdateCategory(this.arouter.snapshot.params.id,fd).subscribe(
          (res) => {this.alert=true,
            console.log(res)},
          (err)=>console.log(err)
        )
        this.router.navigate(['admin/addcategory']);
      }
      closeAlert(){
        this.alert=false
      }
}

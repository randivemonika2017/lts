import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm:any= FormGroup;
  submitted = false;
  returnUrl: any;
  error:any= {};
  loginError: any;
  message=false;
  Role: any = ['Sale-Admin', 'Admin'];
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthService
    ) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      role:['',Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required,Validators.minLength(6)]
    });

    this.authService.logout();
  }
  changeRole(e:any) {
    console.log(e.value)
    this.Role.setValue(e.target.value, {
      onlySelf: true
    })
  }
  get role() {
    return this.loginForm.get('role');
  }
  
  get email() { return this.loginForm.get('email'); }
  get password() { return this.loginForm.get('password'); }
  get f() { return this.loginForm.controls; }
  onSubmit() {
     this.message = true;
    this.authService.login(this.email.value, this.password.value,this.role.value).subscribe((data:any) => {

      if(data.data.status =="success" && data.data.role =="Admin"){
      localStorage.setItem("token",data.data.token);
        this.router.navigate(['/admin']);
      }
      else if(data.data.status =="success" && data.data.role =="Sale-Admin"){
        localStorage.setItem("token",data.data.token);
        this.router.navigate(['/salesadmin']);
      }
      console.log(data);
     
      },
      error => this.error = error
    );
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthRoutingModule } from './auth-routing.module';
//import { LoginComponent } from './login/login.component';
import { FormBuilder, ReactiveFormsModule  } from '@angular/forms';
import { SignupComponent } from '../admin/signup/signup.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { LoginComponent } from './login/login.component';



@NgModule({
  declarations: [LoginComponent,SignupComponent,ForgotPasswordComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AuthRoutingModule
  ],
  providers:[FormBuilder]
})
export class AuthModule { }

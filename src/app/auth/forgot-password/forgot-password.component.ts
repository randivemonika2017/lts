import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  forgotpasswordForm:any= FormGroup;
 // submitted = false;
  returnUrl: any;
  error:any= {};
  loginError: any;
  message=false;
  Role: any = ['Sale-Admin', 'Admin'];
  constructor(
    private fb: FormBuilder,
    private router: Router,
    private authService: AuthService
    ) { }

  ngOnInit() {
    this.forgotpasswordForm = this.fb.group({
      role:['',Validators.required],
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    this.authService.logout();
  }
  changeRole(e:any) {
    console.log(e.value)
    this.Role.setValue(e.target.value, {
      onlySelf: true
    })
  }
  get role() {
    return this.forgotpasswordForm.get('role');
  }
  get email() { return this.forgotpasswordForm.get('email'); }
 

  onSubmit() {
//      this.message = true;
//     this.authService.login(this.email.value, this.password.value).subscribe((data:any) => {
//       //  if (this.authService.isLoggedIn()) {
//       //     const redirect = this.authService.redirectUrl ? this.authService.redirectUrl : '/admin';
//       //     this.router.navigate([redirect]);
//       //   } else {
//       //     this.loginError = 'Username or password is incorrect.';
//       //   }


//       console.log(data);

//       if(data.message == "true"){
//       this.router.navigate(['/admin1']);
//       }
//       },
//       error => this.error = error
//     );
//   }
// }

}
}
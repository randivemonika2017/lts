import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  //serverUrl = 'http://ydbjewellersserver.jkgroupmanpower.in/public/';
  serverUrl='http://localtradestreet.com/ltsapi/public/api/';
  errorData:any= {} ;
  currentUser: any;

  constructor(private http: HttpClient) { }

  redirectUrl: any ;

  login(email: string, password: string,role:string) {
    return this.http.post<any>(`${this.serverUrl}login`, {email: email, password: password,role:role});
  }
  // saleslogin(sp_email: string, sp_password: string) {
  //   return this.http.post<any>(`${this.serverUrl}login`, {sp_email: sp_email, sp_password: sp_password});
  // }


  isLoggedIn() {
    if (localStorage.getItem('currentUser')) {
      return true;
    }
    return false;
  }

  getAuthorizationToken() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser') || '{}');
    return this.currentUser.token;
  }

  logout() {
    localStorage.removeItem('currentUser');
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {

      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {

      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
    }

    // return an observable with a user-facing error message
    this.errorData = {
      errorTitle: 'Oops! Request for document failed',
      errorDesc: 'Something bad happened. Please try again later.'
    };
    return throwError(this.errorData);
  }
}

import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class HidebannerService {

  constructor() { }
  bannerSource = new BehaviorSubject(true);
  bannerObserver = this.bannerSource.asObservable();
}

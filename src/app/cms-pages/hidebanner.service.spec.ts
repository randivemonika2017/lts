import { TestBed } from '@angular/core/testing';

import { HidebannerService } from './hidebanner.service';

describe('HidebannerService', () => {
  let service: HidebannerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HidebannerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

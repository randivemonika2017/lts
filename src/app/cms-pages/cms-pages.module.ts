import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CmsPagesRoutingModule } from './cms-pages-routing.module';
import { AboutComponent } from './about/about.component';
import { CareerComponent } from './career/career.component';
import { ContactComponent } from './contact/contact.component';


@NgModule({
  declarations: [AboutComponent, CareerComponent, ContactComponent],
  imports: [
    CommonModule,
    CmsPagesRoutingModule
  ]
})
export class CmsPagesModule { }

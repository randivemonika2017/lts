import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { YouTubePlayer } from '@angular/youtube-player';
import { environment } from 'src/environments/environment';
import { DataService } from '../customer/data.service';

@Component({
  selector: 'app-business-profile',
  templateUrl: './business-profile.component.html',
  styleUrls: ['./business-profile.component.css']
})
export class BusinessProfileComponent implements OnInit {
  para: any;
  businessprofile: any=[];
  url = environment;
  banner: any;
//  images = [];


  constructor(private dataService:DataService,private router:Router,private route:ActivatedRoute) { 
  
    this.route.params.subscribe((dt)=>{
      this.para = dt;
      console.log(this.para);
    })
  }

  

  ngOnInit() {
    this.dataService.getBusiness(this.para.title).subscribe(
      (data) => {
        
        this.businessprofile = data;
        console.log(this.businessprofile[0].video_url)
      this.banner=this.url.img_url+'/Business/banner/'+this.businessprofile[0].banner;
   
      console.log(this.banner);
      }
    );

  }

}
